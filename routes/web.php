<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('frontend.beranda');
// });

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//frontend
Route::get('/', 'FrontEndController@index');
Route::get('/detail-artikel/{url}/{id}', 'FrontEndController@detail_artikel');


//backend
Route::get('/admin', 'BackEndController@index')->middleware('admin');
//sub-kategori
Route::get('/sub-kategori', 'SubKategoriController@index')->middleware('admin');
Route::get('/sub-kategori-tambah', 'SubKategoriController@create')->middleware('admin');
Route::post('/sub-kategori-tambah/input', 'SubKategoriController@store')->middleware('admin');
Route::get('/sub-kategori-edit/{id}', 'SubKategoriController@edit')->middleware('admin');
Route::post('/sub-kategori-edit/update/{id}', 'SubKategoriController@update')->middleware('admin');
Route::get('/sub-kategori-delete/{id}', 'SubKategoriController@destroy')->middleware('admin');
//kategori
Route::get('/kategori', 'KategoriController@index')->middleware('admin');
Route::get('/kategori-tambah', 'KategoriController@create')->middleware('admin');
Route::post('/kategori-tambah/input', 'KategoriController@store')->middleware('admin');
Route::get('/kategori-edit/{id}', 'KategoriController@edit')->middleware('admin');
Route::post('/kategori-edit/update/{id}', 'KategoriController@update')->middleware('admin');
Route::get('/kategori-delete/{id}', 'KategoriController@destroy')->middleware('admin');
//penulis ARTIKEL
Route::get('/penulis', 'PenulisController@index')->middleware('admin');
Route::get('/penulis-tambah', 'PenulisController@create')->middleware('admin');
Route::post('/penulis-tambah/input', 'PenulisController@store')->middleware('admin');
Route::get('/penulis-edit/{id}', 'PenulisController@edit')->middleware('admin');
Route::post('/penulis-edit/update/{id}', 'PenulisController@update')->middleware('admin');
Route::get('/penulis-delete/{id}', 'PenulisController@destroy')->middleware('admin');
//editor VIDEO
Route::get('/editor', 'EditorController@index')->middleware('admin');
Route::get('/editor-tambah', 'EditorController@create')->middleware('admin');
Route::post('/editor-tambah/input', 'EditorController@store')->middleware('admin');
Route::get('/editor-edit/{id}', 'EditorController@edit')->middleware('admin');
Route::post('/editor-edit/update/{id}', 'EditorController@update')->middleware('admin');
Route::get('/editor-delete/{id}', 'EditorController@destroy')->middleware('admin');
//artikel
Route::get('/artikel', 'ArtikelController@index')->middleware('admin');
Route::get('/artikel-tambah', 'ArtikelController@create')->middleware('admin');
Route::post('/artikel-tambah/input', 'ArtikelController@store')->middleware('admin');
Route::get('/artikel-edit/{id}', 'ArtikelController@edit')->middleware('admin');
Route::post('/artikel-edit/update/{id}', 'ArtikelController@update')->middleware('admin');
Route::get('/artikel-delete/{id}', 'ArtikelController@destroy')->middleware('admin');
//artikel video
Route::get('/artikel-video', 'ArtikelVideoController@index')->middleware('admin');
Route::get('/artikel-video-tambah', 'ArtikelVideoController@create')->middleware('admin');
Route::post('/artikel-video-tambah/input', 'ArtikelVideoController@store')->middleware('admin');
Route::get('/artikel-video-edit/{id}', 'ArtikelVideoController@edit')->middleware('admin');
Route::post('/artikel-video-edit/update/{id}', 'ArtikelVideoController@update')->middleware('admin');
Route::get('/artikel-video-delete/{id}', 'ArtikelVideoController@destroy')->middleware('admin');
//Galeri
Route::get('/galeri', 'GaleriController@index')->middleware('admin');
Route::get('/galeri-tambah', 'GaleriController@create')->middleware('admin');
Route::post('/galeri-tambah/input', 'GaleriController@store')->middleware('admin');
Route::get('/galeri-edit/{id}', 'GaleriController@edit')->middleware('admin');
Route::post('/galeri-edit/update/{id}', 'GaleriController@update')->middleware('admin');
Route::get('/galeri-delete/{id}', 'GaleriController@destroy')->middleware('admin');
//kegiatan
Route::get('/kegiatan', 'KegiatanController@index')->middleware('admin');
Route::get('/kegiatan-tambah', 'KegiatanController@create')->middleware('admin');
Route::post('/kegiatan-tambah/input', 'KegiatanController@store')->middleware('admin');
Route::get('/kegiatan-edit/{id}', 'KegiatanController@edit')->middleware('admin');
Route::post('/kegiatan-edit/update/{id}', 'KegiatanController@update')->middleware('admin');
Route::get('/kegiatan-delete/{id}', 'KegiatanController@destroy')->middleware('admin');
//tabloid Buhun
Route::get('/tabloid-buhun', 'BuhunController@index')->middleware('admin');
Route::get('/tabloid-buhun-tambah', 'BuhunController@create')->middleware('admin');
Route::post('/tabloid-buhun-tambah/input', 'BuhunController@store')->middleware('admin');
Route::get('/tabloid-buhun-edit/{id}', 'BuhunController@edit')->middleware('admin');
Route::post('/tabloid-buhun-edit/update/{id}', 'BuhunController@update')->middleware('admin');
Route::get('/tabloid-buhun-delete/{id}', 'BuhunController@destroy')->middleware('admin');
//perpustakaan
Route::get('/perpustakaan', 'PerpusController@index')->middleware('admin');
Route::get('/perpustakaan-tambah', 'PerpusController@create')->middleware('admin');
Route::post('/perpustakaan-tambah/input', 'PerpusController@store')->middleware('admin');
Route::get('/perpustakaan-edit/{id}', 'PerpusController@edit')->middleware('admin');
Route::post('/perpustakaan-edit/update/{id}', 'PerpusController@update')->middleware('admin');
Route::get('/perpustakaan-delete/{id}', 'PerpusController@destroy')->middleware('admin');
//info_web
Route::get('/info-kontak', 'InfoWebController@index')->middleware('admin');
Route::get('info-kontak-edit/{id}', 'InfoWebController@edit')->middleware('admin');
Route::post('info-kontak-edit/update/{id}', 'InfoWebController@update')->middleware('admin');
//loginPost
Route::get('/login', 'Authentication@login');
Route::post('/loginPost', 'Authentication@loginPost');
Route::get('/register', 'Authentication@register');
Route::post('/registerPost', 'Authentication@registerPost');
Route::get('/logout', 'Authentication@logout');
