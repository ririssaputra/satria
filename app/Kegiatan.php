<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Kegiatan extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $table = 'kegiatan';

    protected $fillable = [
        'id_kegiatan',
        'nama_keg_id',
        'foto_keg1',
        'isi_keg_id',
        'foto_keg2',
        'tgl_kegiatan',
        'updated_at',
        'created_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    protected $hidden = [
        'id_kegiatan',
    ];
}
