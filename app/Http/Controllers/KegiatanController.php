<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use Carbon\Carbon;
use Redirect;
use DataTables;
use Auth;
use Mail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Kegiatan;
use Str;
use Illuminate\Support\Facades\Session;

class KegiatanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $title = "Admin | Kegiatan S3";
      $menubar = "kegiatan";
      $active  = "";

      $tampilKegiatan = DB::table('kegiatan')
                      ->get();

      return view('backend.kegiatan.index', ['menubar' => $menubar, 'active' => $active, 'title' => $title, 'tampilKegiatan' => $tampilKegiatan]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $title = "Admin | Kegiatan";
      $menubar = "kegiatan";
      $active  = "";

      return view('backend.kegiatan.tambah', ['title' => $title, 'menubar' => $menubar, 'active' => $active]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request, [
      'gambar'  => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
      'gambar2' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
      ]);

      $input = $request->all();

      $pesanError = array();
      if (trim($input['txtNamaKegiatan'])=="") {
          $pesanError[] = "Nama Kegiatan tidak boleh kosong !";
      }
      if (trim($input['txtIsi'])=="") {
          $pesanError[] = "Isi tidak boleh kosong !";
      }
      if (trim($input['txtTgl'])=="") {
          $pesanError[] = "Tanggal Kegiatan tidak boleh kosong !";
      }


      if (count($pesanError)>=1 ){
        $data = '';
        for($i=0;$i<count($pesanError);$i++){
          $data.=  $pesanError[$i].',';
        }
          return redirect('/kegiatan-tambah')->with('pesanError',$data);
      }else{

        function UploadImage($fupload_name){
        //direktori gambar
        $vdir_upload = "assets/gambar_kegiatan/";
        $vfile_upload = $vdir_upload . $fupload_name;

        //Simpan gambar dalam ukuran sebenarnya
        move_uploaded_file($_FILES["gambar"]["tmp_name"], $vfile_upload);

          $typeUpload = $_FILES["gambar"]["type"];

          if ($typeUpload == 'image/jpeg'){
          $im_src = imagecreatefromjpeg($vfile_upload);
          }else{
          $im_src = imagecreatefrompng($vfile_upload);
          }
        $src_width = imageSX($im_src);
        $src_height = imageSY($im_src);

        //Simpan dalam versi small 110 pixel
        //Set ukuran gambar hasil perubahan
        $dst_width = 580;
        $dst_height = ($dst_width/$src_width)*$src_height;

        //proses perubahan ukuran
        $im = imagecreatetruecolor($dst_width,$dst_height);
        imagecopyresampled($im, $im_src, 0, 0, 0, 0, $dst_width, $dst_height, $src_width, $src_height);

        //Simpan gambar
        imagejpeg($im,$vdir_upload . "small_" . $fupload_name);

        //Hapus gambar di memori komputer
        imagedestroy($im_src);
        imagedestroy($im);
        }

        $lokasi_file    = $_FILES['gambar']['tmp_name'];
        $tipe_file      = $_FILES['gambar']['type'];
        $nama_file      = $_FILES['gambar']['name'];
        $acak           = rand(1,999);
        $nama_file_unik = $acak.$nama_file;

        function UploadImage2($fupload_name2){
        //direktori gambar
        $vdir_upload = "assets/gambar_kegiatan/";
        $vfile_upload = $vdir_upload . $fupload_name2;

        //Simpan gambar dalam ukuran sebenarnya
        move_uploaded_file($_FILES["gambar2"]["tmp_name"], $vfile_upload);

          $typeUpload = $_FILES["gambar2"]["type"];

          //identitas file asli
          if ($typeUpload == 'image/jpeg'){
          $im_src = imagecreatefromjpeg($vfile_upload);
          }else{
          $im_src = imagecreatefrompng($vfile_upload);
          }
        $src_width = imageSX($im_src);
        $src_height = imageSY($im_src);

        //Simpan dalam versi small 110 pixel
        //Set ukuran gambar hasil perubahan
        $dst_width = 580;
        $dst_height = ($dst_width/$src_width)*$src_height;

        //proses perubahan ukuran
        $im = imagecreatetruecolor($dst_width,$dst_height);
        imagecopyresampled($im, $im_src, 0, 0, 0, 0, $dst_width, $dst_height, $src_width, $src_height);

        //Simpan gambar
        imagejpeg($im,$vdir_upload . "small_" . $fupload_name2);

        //Hapus gambar di memori komputer
        imagedestroy($im_src);
        imagedestroy($im);
        }

        $lokasi_file2    = $_FILES['gambar2']['tmp_name'];
        $tipe_file2      = $_FILES['gambar2']['type'];
        $nama_file2      = $_FILES['gambar2']['name'];
        $acak2           = rand(1,999);
        $nama_file_unik2 = $acak2.$nama_file2;

        $tambah_tanggal = mktime(0,0,0,date('m')+0,date('d')+0,date('Y')+1);

        $tanggalAktif = date('Y-m-d',$tambah_tanggal);

        if (!empty($lokasi_file)){
          UploadImage($nama_file_unik);
          $linkhosting	= 'http://localhost:8000/assets/gambar_kegiatan/small_'.$nama_file_unik;
        }else{
          $nama_file_unik = 'noimage.png';
          $linkhosting	= 'http://localhost:8000/assets/gambar_kegiatan/small_noimage.png';
        }

        if (!empty($lokasi_file2)){
          UploadImage2($nama_file_unik2);
          $linkhosting2	= 'http://localhost:8000/assets/gambar_kegiatan/small_'.$nama_file_unik2;
        }else{
          $nama_file_unik = 'noimage.png';
          $linkhosting2	= 'http://localhost:8000/assets/gambar_kegiatan/small_noimage.png';
        }


        $register = Kegiatan::create([
          'nama_keg_id'          => $input['txtNamaKegiatan'],
          // 'nama_keg_en'          => $input['txtNamaKegiatan2'],
          'foto_keg1'   => $nama_file_unik,
          'foto_keg2'   => $nama_file_unik2,
          'isi_keg_id'   => $input['txtIsi'],
          // 'isi_keg_en'   => $input['txtIsi2'],
          'tgl_kegiatan' => $input['txtTgl'],
          'created_at'        => Carbon::now(),
          'updated_at'        => Carbon::now(),
        ]);
      }


      $msg = 'Kegiatan berhasil di input';

      return redirect('kegiatan')->with('msg',$msg);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $title = "Admin | Kegiatan";
      $menubar = "kegiatan";
      $active  = "";

      $tampilGaleri = DB::table('kegiatan')->where('id_kegiatan', '=', $id)->get();

      $pesanError = array();

      if (!isset($tampilGaleri[0])) {
          $pesanError[] = "Data tidak ada !";
      }

      if (count($pesanError)>=1 ){
          return redirect('/kegiatan-edit')->with('pesanUpdateError',$pesanError);
      }

      $hasilId = $tampilGaleri[0]->id_kegiatan;
      return view('backend.kegiatan.edit', ['title' => $title, 'menubar' => $menubar, 'active' => $active, 'tampilGaleri' => $tampilGaleri, 'hasilId' => $hasilId]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $input = $request->all();

      $pesanError = array();
      if (trim($input['txtNamaKegiatan'])=="") {
          $pesanError[] = "Nama Kegiatan tidak boleh kosong !";
      }
      if (trim($input['txtIsi'])=="") {
          $pesanError[] = "Isi tidak boleh kosong !";
      }
      if (trim($input['txtTgl'])=="") {
          $pesanError[] = "Tanggal Kegiatan tidak boleh kosong !";
      }


      if (count($pesanError)>=1 ){
          return redirect('kegiatan-edit/'.$id)->with('pesanError',$pesanError);
      }else{

        function UploadImage($fupload_name){
        //direktori gambar
        $vdir_upload = "assets/gambar_kegiatan/";
        $vfile_upload = $vdir_upload . $fupload_name;

        //Simpan gambar dalam ukuran sebenarnya
        move_uploaded_file($_FILES["gambar"]["tmp_name"], $vfile_upload);

          $typeUpload = $_FILES["gambar"]["type"];

          if ($typeUpload == 'image/jpeg'){
          $im_src = imagecreatefromjpeg($vfile_upload);
          }else{
          $im_src = imagecreatefrompng($vfile_upload);
          }
        $src_width = imageSX($im_src);
        $src_height = imageSY($im_src);

        //Simpan dalam versi small 110 pixel
        //Set ukuran gambar hasil perubahan
        $dst_width = 580;
        $dst_height = ($dst_width/$src_width)*$src_height;

        //proses perubahan ukuran
        $im = imagecreatetruecolor($dst_width,$dst_height);
        imagecopyresampled($im, $im_src, 0, 0, 0, 0, $dst_width, $dst_height, $src_width, $src_height);

        //Simpan gambar
        imagejpeg($im,$vdir_upload . "small_" . $fupload_name);

        //Hapus gambar di memori komputer
        imagedestroy($im_src);
        imagedestroy($im);
        }

        $lokasi_file    = $_FILES['gambar']['tmp_name'];
        $tipe_file      = $_FILES['gambar']['type'];
        $nama_file      = $_FILES['gambar']['name'];
        $acak           = rand(1,999);
        $nama_file_unik = $acak.$nama_file;

        function UploadImage2($fupload_name2){
        //direktori gambar
        $vdir_upload = "assets/gambar_kegiatan/";
        $vfile_upload = $vdir_upload . $fupload_name2;

        //Simpan gambar dalam ukuran sebenarnya
        move_uploaded_file($_FILES["gambar2"]["tmp_name"], $vfile_upload);

          $typeUpload = $_FILES["gambar2"]["type"];

          //identitas file asli
          if ($typeUpload == 'image/jpeg'){
          $im_src = imagecreatefromjpeg($vfile_upload);
          }else{
          $im_src = imagecreatefrompng($vfile_upload);
          }
        $src_width = imageSX($im_src);
        $src_height = imageSY($im_src);

        //Simpan dalam versi small 110 pixel
        //Set ukuran gambar hasil perubahan
        $dst_width = 580;
        $dst_height = ($dst_width/$src_width)*$src_height;

        //proses perubahan ukuran
        $im = imagecreatetruecolor($dst_width,$dst_height);
        imagecopyresampled($im, $im_src, 0, 0, 0, 0, $dst_width, $dst_height, $src_width, $src_height);

        //Simpan gambar
        imagejpeg($im,$vdir_upload . "small_" . $fupload_name2);

        //Hapus gambar di memori komputer
        imagedestroy($im_src);
        imagedestroy($im);
        }

        $lokasi_file2    = $_FILES['gambar2']['tmp_name'];
        $tipe_file2      = $_FILES['gambar2']['type'];
        $nama_file2      = $_FILES['gambar2']['name'];
        $acak2           = rand(1,999);
        $nama_file_unik2 = $acak2.$nama_file2;


        $tambah_tanggal = mktime(0,0,0,date('m')+0,date('d')+0,date('Y')+1);

        $tanggalAktif = date('Y-m-d',$tambah_tanggal);

        if (!empty($lokasi_file)){
          if ($request->hidgambar!='noimage.png'){
          @unlink('assets/gambar_kegiatan/'.$request->hidgambar);
          @unlink('assets/gambar_kegiatan/'.'small_'.$request->hidgambar);
          }
          UploadImage($nama_file_unik);
          $linkhosting	= 'http://127.0.0.1:8000/assets/gambar_kegiatan/small_'.$nama_file_unik;
        }else{
          $nama_file_unik = $request->hidgambar;
          $linkhosting	= 'http://127.0.0.1:8000/assets/gambar_kegiatan/small_'.$request->hidgambar;
        }

        if (!empty($lokasi_file2)){
          if ($request->hidgambar2!='noimage.png'){
          @unlink('assets/gambar_kegiatan/'.$request->hidgambar2);
          @unlink('assets/gambar_kegiatan/'.'small_'.$request->hidgambar2);
          }
          UploadImage2($nama_file_unik2);
          $linkhosting2	= 'http://127.0.0.1:8000/assets/gambar_kegiatan/small_'.$nama_file_unik2;
        }else{
          $nama_file_unik2 = $request->hidgambar2;
          $linkhosting2	= 'http://127.0.0.1:8000/assets/gambar_kegiatan/small_'.$request->hidgambar2;
        }


        $data = ['nama_keg_id'        => $input['txtNamaKegiatan'],
                 // 'nama_keg_en'        => $input['txtNamaKegiatan2'],
                 'foto_keg1'   => $nama_file_unik,
                 'foto_keg2'   => $nama_file_unik2,
                 'isi_keg_id' => $input['txtIsi'],
                 // 'isi_keg_en' => $input['txtIsi2'],
                 'tgl_kegiatan' => $input['txtTgl'],
                 'updated_at'      => Carbon::now(),
                ];
    DB::table('kegiatan')->where('id_kegiatan',$id)->update($data);

  }

    $msg = 'Data berhasil di perbaharui';

  return redirect('kegiatan')->with('msg',$msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $hasilCostumers=DB::table('kegiatan')->where('id_kegiatan', '=', $id)->get();
      //dd($hasilUsers);
      $pesanError = array();

      if (!isset($hasilCostumers[0])) {
          $pesanError[] = "Data tidak ada !";
      }

      if (count($pesanError)>=1 ){
          return redirect('/kegiatan')->with('pesanUpdateError',$pesanError);
      }else{

        DB::table('kegiatan')->where('id_kegiatan', '=', $id)->delete();
        $users = DB::table('kegiatan')->simplePaginate(10);

        $msg = 'Data berhasil di hapus';

        return redirect('/kegiatan')->with('msg',$msg);;
      }
    }
}
