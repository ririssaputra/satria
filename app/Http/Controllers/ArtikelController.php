<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use Carbon\Carbon;
use Redirect;
use DataTables;
use Auth;
use Mail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Artikel;
use Str;
use Illuminate\Support\Facades\Session;
// use App\Point;

use View;

class ArtikelController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth:admin');
    // }
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = "Admin | Artikel";
        $menubar = "berita";
        $active  = "artikel";

        $tampilArtikel = DB::table('artikel')
                        ->select('artikel.*', 'kategori.*','sub_kategori.*')
                        ->leftjoin('kategori', 'artikel.id_kategori', '=', 'kategori.id_kategori')
                        ->leftjoin('sub_kategori', 'artikel.id_sub_kategori', '=', 'sub_kategori.id_sub_kategori')
                        ->orderBy('artikel.id','desc')
                        ->get();

        return view('backend.artikel.index', ['menubar' => $menubar, 'active' => $active, 'title' => $title, 'tampilArtikel' => $tampilArtikel]);
    }

    public function load_sub_menu($id)
    {
        $tampilSubKategori = DB::table('sub_kategori')
                              ->where('id_kategori', '=', $id)
                              ->orderBy('nama_sub_kategori','ASC')
                              ->get();

        $submenu = "<select name='txtSubKategori2' class='form-control'>";
        $submenu .= "<option value=''>-- Pilih Sub Kategori Artikel --</option>";
        foreach ($tampilSubKategori as $item) {
        $submenu .= "<option value='".$item->id_sub_kategori."'>".$item->nama_sub_kategori."</option>";
        }
        $submenu .= "</select>";

        return $submenu;
    }

    public function create()
    {
        $title = "Admin | Artikel";
        $menubar = "berita";
        $active  = "artikel";

        $tampilKategori = DB::table('kategori')
                              ->orderBy('nama_kategori','ASC')
                              ->get();

        $tampilPenulis = DB::table('penulis')
                         ->orderBy('nama_penulis','ASC')
                         ->get();

        return view('backend.artikel.tambah', ['title' => $title, 'menubar' => $menubar, 'active' => $active, 'tampilKategori' => $tampilKategori, 'tampilPenulis' => $tampilPenulis]);
    }

    public function store(Request $request)
    {

        $this->validate($request, [
        'gambar'  => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        'gambar2' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $input = $request->all();

        $pesanError = array();
        if (trim($input['txtJudul'])=="") {
            $pesanError[] = "Judul tidak boleh kosong !";
        }
        if (trim($input['txtIsi'])=="") {
            $pesanError[] = "Isi tidak boleh kosong !";
        }
        if (trim($input['txtKategori'])=="") {
            $pesanError[] = "Kategori tidak boleh kosong !";
        }


        if (count($pesanError)>=1 ){
            return redirect('artikel-tambah')->with('pesanError',$pesanError);
        }else{

          function UploadImage($fupload_name){
          //direktori gambar
          $vdir_upload = "assets/gambar_artikel/";
          $vfile_upload = $vdir_upload . $fupload_name;

          //Simpan gambar dalam ukuran sebenarnya
          move_uploaded_file($_FILES["gambar"]["tmp_name"], $vfile_upload);

            $typeUpload = $_FILES["gambar"]["type"];

            if ($typeUpload == 'image/jpeg'){
            $im_src = imagecreatefromjpeg($vfile_upload);
            }else{
            $im_src = imagecreatefrompng($vfile_upload);
            }
          $src_width = imageSX($im_src);
          $src_height = imageSY($im_src);

          //Simpan dalam versi small 110 pixel
          //Set ukuran gambar hasil perubahan
          $dst_width = 580;
          $dst_height = ($dst_width/$src_width)*$src_height;

          //proses perubahan ukuran
          $im = imagecreatetruecolor($dst_width,$dst_height);
          imagecopyresampled($im, $im_src, 0, 0, 0, 0, $dst_width, $dst_height, $src_width, $src_height);

          //Simpan gambar
          imagejpeg($im,$vdir_upload . "small_" . $fupload_name);

          //Hapus gambar di memori komputer
          imagedestroy($im_src);
          imagedestroy($im);
          }

          $lokasi_file    = $_FILES['gambar']['tmp_name'];
          $tipe_file      = $_FILES['gambar']['type'];
          $nama_file      = $_FILES['gambar']['name'];
          $acak           = rand(1,999);
          $nama_file_unik = $acak.$nama_file;

          function UploadImage2($fupload_name2){
          //direktori gambar
          $vdir_upload = "assets/gambar_artikel/";
          $vfile_upload = $vdir_upload . $fupload_name2;

          //Simpan gambar dalam ukuran sebenarnya
          move_uploaded_file($_FILES["gambar2"]["tmp_name"], $vfile_upload);

            $typeUpload = $_FILES["gambar2"]["type"];

            //identitas file asli
            if ($typeUpload == 'image/jpeg'){
            $im_src = imagecreatefromjpeg($vfile_upload);
            }else{
            $im_src = imagecreatefrompng($vfile_upload);
            }
          $src_width = imageSX($im_src);
          $src_height = imageSY($im_src);

          //Simpan dalam versi small 110 pixel
          //Set ukuran gambar hasil perubahan
          $dst_width = 580;
          $dst_height = ($dst_width/$src_width)*$src_height;

          //proses perubahan ukuran
          $im = imagecreatetruecolor($dst_width,$dst_height);
          imagecopyresampled($im, $im_src, 0, 0, 0, 0, $dst_width, $dst_height, $src_width, $src_height);

          //Simpan gambar
          imagejpeg($im,$vdir_upload . "small_" . $fupload_name2);

          //Hapus gambar di memori komputer
          imagedestroy($im_src);
          imagedestroy($im);
          }

          $lokasi_file2    = $_FILES['gambar2']['tmp_name'];
          $tipe_file2      = $_FILES['gambar2']['type'];
          $nama_file2      = $_FILES['gambar2']['name'];
          $acak2           = rand(1,999);
          $nama_file_unik2 = $acak2.$nama_file2;

          $tambah_tanggal = mktime(0,0,0,date('m')+0,date('d')+0,date('Y')+1);

          $tanggalAktif = date('Y-m-d',$tambah_tanggal);

          if (!empty($lokasi_file)){
            UploadImage($nama_file_unik);
            $linkhosting	= 'http://localhost:8000/assets/gambar_artikel/small_'.$nama_file_unik;
          }else{
            $nama_file_unik = 'noimage.png';
            $linkhosting	= 'http://localhost:8000/assets/gambar_artikel/small_noimage.png';
          }

          if (!empty($lokasi_file2)){
            UploadImage2($nama_file_unik2);
            $linkhosting2	= 'http://localhost:8000/assets/gambar_artikel/small_'.$nama_file_unik2;
          }else{
            $nama_file_unik = 'noimage.png';
            $linkhosting2	= 'http://localhost:8000/assets/gambar_artikel/small_noimage.png';
          }

          $kode  = 'ARTIKEL';
          $index = DB::table('artikel')
                            ->orderBy('id','DESC')
                            ->get();

          $tglSekarang = date('Y-m-d');

          if (!isset($index[0])) {
              $newid = $kode.'00001';
          } else{
              $index = $index[0]->id + 1;
              $a     = str_pad($index, 4, "0", STR_PAD_LEFT);
              $newid = $kode.'-'.$tglSekarang.'-'.$a;
          }

          $register = Artikel::create([
            'id_artikel'        => $newid,
            'judul_id'          => $input['txtJudul'],
            'judul_en'          => $input['txtJudul2'],
            'gambar_artikel1'   => $nama_file_unik,
            'gambar_artikel2'   => $nama_file_unik2,
            'info_gambar1'      => $input['txtInfo'],
            'info_gambar2'      => $input['txtInfo2'],
            'isi_artikel_id1'   => $input['txtIsi'],
            'isi_artikel_id2'   => $input['txtIsi2'],
            'isi_artikel_en1'   => $input['txtIsi3'],
            'isi_artikel_en2'   => $input['txtIsi4'],
            'keyword'           => $input['txtMeta'],
            'slug1'             => Str::slug($input['txtJudul']),
            'slug2'             => Str::slug($input['txtJudul2']),
            'hitung_tayang'     => '0',
            'status_publish'    => '1',
            'id_kategori'       => $input['txtKategori'],
            'id_penulis'        => $input['txtPenulis'],
            'id_user'           => Session::get('id'),
            'created_at'        => Carbon::now(),
            'updated_at'        => Carbon::now(),
          ]);
        }


        $msg = 'Artikel berhasil di input';

        return redirect('artikel')->with('msg',$msg);

    }

	public function edit($id)
    {
      $title = "Admin | Artikel";
      $menubar = "berita";
      $active  = "artikel";

      $tampilArtikel = DB::table('artikel')->where('id_artikel', '=', $id)->get();

      $pesanError = array();

      if (!isset($tampilArtikel[0])) {
          $pesanError[] = "Data tidak ada !";
      }

      if (count($pesanError)>=1 ){
          return redirect('artikel/artikel-edit')->with('pesanUpdateError',$pesanError);
      }else{

          $tampilKategori = DB::table('kategori')
                          ->get();

		  $hasilId = $tampilArtikel[0]->id_artikel;
		  return view('backend.artikel.edit', ['title' => $title, 'menubar' => $menubar, 'active' => $active, 'tampilArtikel' => $tampilArtikel, 'hasilId' => $hasilId, 'tampilKategori' => $tampilKategori]);
      }
    }

    // Update costumer
    public function update(Request $request, $id)
    {
      $input = $request->all();

      $pesanError = array();
      if (trim($input['txtJudul'])=="") {
          $pesanError[] = "Judul tidak boleh kosong !";
      }
      if (trim($input['txtIsi'])=="") {
          $pesanError[] = "Isi tidak boleh kosong !";
      }
      if (trim($input['txtKategori'])=="") {
          $pesanError[] = "Kategori tidak boleh kosong !";
      }


      if (count($pesanError)>=1 ){
          return redirect('artikel-edit/'.$id)->with('pesanError',$pesanError);
      }else{

        function UploadImage($fupload_name){
        //direktori gambar
        $vdir_upload = "assets/gambar_artikel/";
        $vfile_upload = $vdir_upload . $fupload_name;

        //Simpan gambar dalam ukuran sebenarnya
        move_uploaded_file($_FILES["gambar"]["tmp_name"], $vfile_upload);

          $typeUpload = $_FILES["gambar"]["type"];

          if ($typeUpload == 'image/jpeg'){
          $im_src = imagecreatefromjpeg($vfile_upload);
          }else{
          $im_src = imagecreatefrompng($vfile_upload);
          }
        $src_width = imageSX($im_src);
        $src_height = imageSY($im_src);

        //Simpan dalam versi small 110 pixel
        //Set ukuran gambar hasil perubahan
        $dst_width = 580;
        $dst_height = ($dst_width/$src_width)*$src_height;

        //proses perubahan ukuran
        $im = imagecreatetruecolor($dst_width,$dst_height);
        imagecopyresampled($im, $im_src, 0, 0, 0, 0, $dst_width, $dst_height, $src_width, $src_height);

        //Simpan gambar
        imagejpeg($im,$vdir_upload . "small_" . $fupload_name);

        //Hapus gambar di memori komputer
        imagedestroy($im_src);
        imagedestroy($im);
        }

        $lokasi_file    = $_FILES['gambar']['tmp_name'];
        $tipe_file      = $_FILES['gambar']['type'];
        $nama_file      = $_FILES['gambar']['name'];
        $acak           = rand(1,999);
        $nama_file_unik = $acak.$nama_file;

        function UploadImage2($fupload_name2){
        //direktori gambar
        $vdir_upload = "assets/gambar_artikel/";
        $vfile_upload = $vdir_upload . $fupload_name2;

        //Simpan gambar dalam ukuran sebenarnya
        move_uploaded_file($_FILES["gambar2"]["tmp_name"], $vfile_upload);

          $typeUpload = $_FILES["gambar2"]["type"];

          //identitas file asli
          if ($typeUpload == 'image/jpeg'){
          $im_src = imagecreatefromjpeg($vfile_upload);
          }else{
          $im_src = imagecreatefrompng($vfile_upload);
          }
        $src_width = imageSX($im_src);
        $src_height = imageSY($im_src);

        //Simpan dalam versi small 110 pixel
        //Set ukuran gambar hasil perubahan
        $dst_width = 580;
        $dst_height = ($dst_width/$src_width)*$src_height;

        //proses perubahan ukuran
        $im = imagecreatetruecolor($dst_width,$dst_height);
        imagecopyresampled($im, $im_src, 0, 0, 0, 0, $dst_width, $dst_height, $src_width, $src_height);

        //Simpan gambar
        imagejpeg($im,$vdir_upload . "small_" . $fupload_name2);

        //Hapus gambar di memori komputer
        imagedestroy($im_src);
        imagedestroy($im);
        }

        $lokasi_file2    = $_FILES['gambar2']['tmp_name'];
        $tipe_file2      = $_FILES['gambar2']['type'];
        $nama_file2      = $_FILES['gambar2']['name'];
        $acak2           = rand(1,999);
        $nama_file_unik2 = $acak2.$nama_file2;


        $tambah_tanggal = mktime(0,0,0,date('m')+0,date('d')+0,date('Y')+1);

        $tanggalAktif = date('Y-m-d',$tambah_tanggal);

        if (!empty($lokasi_file)){
          if ($request->hidgambar!='noimage.png'){
          @unlink('assets/gambar_artikel/'.$request->hidgambar);
	        @unlink('assets/gambar_artikel/'.'small_'.$request->hidgambar);
          }
          UploadImage($nama_file_unik);
          $linkhosting	= 'http://127.0.0.1:8000/assets/gambar_artikel/small_'.$nama_file_unik;
        }else{
          $nama_file_unik = $request->hidgambar;
          $linkhosting	= 'http://127.0.0.1:8000/assets/gambar_artikel/small_'.$request->hidgambar;
        }

        if (!empty($lokasi_file2)){
          if ($request->hidgambar2!='noimage.png'){
          @unlink('assets/gambar_artikel/'.$request->hidgambar2);
	        @unlink('assets/gambar_artikel/'.'small_'.$request->hidgambar2);
          }
          UploadImage2($nama_file_unik2);
          $linkhosting2	= 'http://127.0.0.1:8000/assets/gambar_artikel/small_'.$nama_file_unik2;
        }else{
          $nama_file_unik2 = $request->hidgambar2;
          $linkhosting2	= 'http://127.0.0.1:8000/assets/gambar_artikel/small_'.$request->hidgambar2;
        }


        $data = ['judul_id'        => $input['txtJudul'],
                 'judul_en'        => $input['txtJudul2'],
                 'gambar_artikel1'   => $nama_file_unik,
                 'gambar_artikel2'   => $nama_file_unik2,
                 'info_gambar1'    => $input['txtInfo'],
                 'info_gambar2'    => $input['txtInfo2'],
                 'isi_artikel_id1' => $input['txtIsi'],
                 'isi_artikel_id2' => $input['txtIsi2'],
                 'isi_artikel_en1' => $input['txtIsi3'],
                 'isi_artikel_en2' => $input['txtIsi4'],
                 'keyword'         => $input['txtMeta'],
                 'slug1'           => Str::slug($input['txtJudul']),
                 'slug2'           => Str::slug($input['txtJudul2']),
                 'id_kategori'     => $input['txtKategori'],
                 'hitung_tayang'   => '0',
                 'status_publish'  => '1',
                 'updated_at'      => Carbon::now(),
				        ];
		DB::table('artikel')->where('id_artikel',$id)->update($data);

  }

    $msg = 'Artikel berhasil di perbaharui';

  return redirect('artikel')->with('msg',$msg);
  }

  public function destroy($id)
  {
    $hasilCostumers=DB::table('artikel')->where('id_artikel', '=', $id)->get();
    //dd($hasilUsers);
    $pesanError = array();

    if (!isset($hasilCostumers[0])) {
        $pesanError[] = "Data tidak ada !";
    }

    if (count($pesanError)>=1 ){
        return redirect('/artikel')->with('pesanUpdateError',$pesanError);
    }else{

      DB::table('artikel')->where('id_artikel', '=', $id)->delete();
      $users = DB::table('artikel')->simplePaginate(10);

      $msg = 'Artikel berhasil di hapus';

      return redirect('/artikel')->with('msg',$msg);;
    }
  }
}
