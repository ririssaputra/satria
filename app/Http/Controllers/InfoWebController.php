<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class InfoWebController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $title = 'Admin | Info Website';
      $menubar = "info";
      $active  = "";

      $tampilInfoWeb = DB::table('info_web')
                        ->get();

      return view('backend.kontak.index', ['menubar' => $menubar, 'active' => $active, 'tampilInfoWeb' => $tampilInfoWeb, 'title' => $title]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $title = 'Admin | Info Website';
      $menubar = "info";
      $active  = "";
      //Find the user object from model if it exists
      $tampilInfoWeb = DB::table('info_web')->where('id_info_web', '=', $id)->get();
      //		  dd($hasilUsers);
      $pesanError = array();

      if (!isset($tampilInfoWeb[0])) {
        $pesanError[] = "Data tidak ada !";
      }

      if (count($pesanError)>=1 ){
        return redirect('/info-kontak-edit')->with('pesanUpdateError',$pesanError);
      }else{

      $hasilId = $tampilInfoWeb[0]->id_info_web;
      return view('backend.kontak.edit', ['menubar' => $menubar, 'active' => $active, 'tampilInfoWeb' => $tampilInfoWeb, 'hasilId' => $hasilId, 'title' => $title]);
      }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $input = $request->all();

    $pesanError = array();
    if (trim($input['txtNoTelp'])=="") {
        $pesanError[] = "No Telpon tidak boleh kosong !";
    }
    if (trim($input['txtNoWa'])=="") {
        $pesanError[] = "No WhatsApp tidak boleh kosong !";
    }
    if (trim($input['txtEmail'])=="") {
        $pesanError[] = "Alamat Email tidak boleh kosong !";
    }
    if (trim($input['txtAlamat'])=="") {
        $pesanError[] = "Alamat Kantor tidak boleh kosong !";
    }


    if (count($pesanError)>=1 ){
        return redirect('sub-kategori-edit/'.$id)->with('pesanError',$pesanError);
    }else{

      $data = [
              'info_telp' => $input['txtNoTelp'],
              'info_wa' => $input['txtNoWa'],
              'info_email' => $input['txtEmail'],
              'info_alamat' => $input['txtAlamat'],
              'info_facebook' => $input['txtFb'],
              'info_instagram' => $input['txtIg'],
              'info_twitter' => $input['txtTwit'],
              'info_youtube' => $input['txtYutub'],
               'created_at' => Carbon::now(),
               'updated_at' => Carbon::now(),
              ];
        DB::table('info_web')->where('id_info_web',$id)->update($data);

      }

        $msg = 'Data Info berhasil di perbaharui';

      return redirect('info-kontak')->with('msg',$msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
