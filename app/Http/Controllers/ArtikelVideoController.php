<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;
use Validator;
use Carbon\Carbon;
use Redirect;
use DataTables;
use Auth;
use Mail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\ArtikelVideo;
use file;
use Str;
use Illuminate\Support\Facades\Session;
// use App\Point;

use View;

class ArtikelVideoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth:admin');
    // }
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = "Admin | Artikel Video";
        $menubar = "artikelvideo";
        $active  = "video";

        $tampilVideo = DB::table('artikel_video')->get();




        return view('backend.artikel_video.index', ['menubar' => $menubar, 'active' => $active, 'title' => $title, 'tampilVideo' => $tampilVideo ]);
    }

    public function mobileindex()
    {
        return view('content_mobile.beranda');
    }

    public function load_beranda()
    {
        $hasilProduk = DB::table('produk')
                          ->get();

        return view('load.beranda', ['hasilProduk' => $hasilProduk]);
    }

    public function load_lihat_detail()
    {

        return view('load.lihat_detail');
    }

    public function keranjang()
    {
        // return '<script type="text/javascript">alert("hello!");</script>';
        return 'test';
    }

    public function transaksi($id)
    {
        $hasilTransaksi = DB::table('point')
                          ->select('point.*','cabangs.*')
                          ->leftJoin('cabangs', 'point.cabang_id', '=', 'cabangs.id')
                          ->where('point.costumer_id','=', $id)
                          ->where('point.type_validasi','=', 1)
                          ->orderBy('point.costumer_id','desc')
                          ->get();

        return view('admin.costumers.transaksi-costumers', ['hasilTransaksi' => $hasilTransaksi]);
    }

    public function create()
    {
      $title = "Admin | Artikel Video";
      $menubar = "artikelvideo";
      $active  = "video";

      $tampilPenulis = DB::table('penulis')->orderBy('nama_penulis','ASC')->get();
      $tampilKategori = DB::table('kategori')->orderBy('nama_kategori','ASC')->get();
      $tampilEditor = DB::table('editor_video')->orderBy('nama_editor','ASC')->get();

        return view('backend.artikel_video.tambah', ['title'=>$title,'menubar' => $menubar, 'active' => $active, 'tampilPenulis'=>$tampilPenulis, 'tampilKategori'=>$tampilKategori, 'tampilEditor'=>$tampilEditor]);
    }

    public function store(Request $request)
    {
        $input = $request->all();

        $pesanError = array();
        if (trim($input['txtJudul'])=="") {
            $pesanError[] = "Judul tidak boleh kosong ";
        }
        if (trim($input['txtIsi'])=="") {
            $pesanError[] = "Isi tidak boleh kosong ";
        }

        if (trim($input['link_video'])=="") {
            $pesanError[] = "Link tidak boleh kosong ";
        }else{
            $linkVideo = $input['link_video'];
        }


        if (count($pesanError)>=1 ){
          $data = '';
          for($i=0;$i<count($pesanError);$i++){
            $data.=  $pesanError[$i].',';
          }
          return redirect('/artikel-video-tambah')->with('pesanError',$data);
        }else{

          function UploadImage($fupload_name){
          //direktori gambar
          $vdir_upload = "assets/gambar_video_artikel/";
          $vfile_upload = $vdir_upload . $fupload_name;

          //Simpan gambar dalam ukuran sebenarnya
          move_uploaded_file($_FILES["gambar"]["tmp_name"], $vfile_upload);

            $typeUpload = $_FILES["gambar"]["type"];

            //identitas file asli
            if ($typeUpload == 'image/jpeg'){
            $im_src = imagecreatefromjpeg($vfile_upload);
            }else{
            $im_src = imagecreatefrompng($vfile_upload);
            }
          $src_width = imageSX($im_src);
          $src_height = imageSY($im_src);

          //Simpan dalam versi small 110 pixel
          //Set ukuran gambar hasil perubahan
          $dst_width = 580;
          $dst_height = ($dst_width/$src_width)*$src_height;

          //proses perubahan ukuran
          $im = imagecreatetruecolor($dst_width,$dst_height);
          imagecopyresampled($im, $im_src, 0, 0, 0, 0, $dst_width, $dst_height, $src_width, $src_height);

          //Simpan gambar
          imagejpeg($im,$vdir_upload . "small_" . $fupload_name);

          //Hapus gambar di memori komputer
          imagedestroy($im_src);
          imagedestroy($im);
          }

          // $lokasi_file    = $_FILES['gambar']['tmp_name'];
          // $tipe_file      = $_FILES['gambar']['type'];
          // $nama_file      = $_FILES['gambar']['name'];
          // $acak           = rand(1,999);
          // $nama_file_unik = $acak.$nama_file;

          function UploadImage2($fupload_name2){
          //direktori gambar
          $vdir_upload = "assets/gambar_video_artikel/";
          $vfile_upload = $vdir_upload . $fupload_name2;

          //Simpan gambar dalam ukuran sebenarnya
          move_uploaded_file($_FILES["gambar2"]["tmp_name"], $vfile_upload);

            $typeUpload = $_FILES["gambar2"]["type"];

            //identitas file asli
            if ($typeUpload == 'image/jpeg'){
            $im_src = imagecreatefromjpeg($vfile_upload);
            }else{
            $im_src = imagecreatefrompng($vfile_upload);
            }
          $src_width = imageSX($im_src);
          $src_height = imageSY($im_src);

          //Simpan dalam versi small 110 pixel
          //Set ukuran gambar hasil perubahan
          $dst_width = 580;
          $dst_height = ($dst_width/$src_width)*$src_height;

          //proses perubahan ukuran
          $im = imagecreatetruecolor($dst_width,$dst_height);
          imagecopyresampled($im, $im_src, 0, 0, 0, 0, $dst_width, $dst_height, $src_width, $src_height);

          //Simpan gambar
          imagejpeg($im,$vdir_upload . "small_" . $fupload_name2);

          //Hapus gambar di memori komputer
          imagedestroy($im_src);
          imagedestroy($im);
          }

          // $lokasi_file2    = $_FILES['gambar2']['tmp_name'];
          // $tipe_file2      = $_FILES['gambar2']['type'];
          // $nama_file2      = $_FILES['gambar2']['name'];
          // $acak2           = rand(1,999);
          // $nama_file_unik2 = $acak2.$nama_file2;

          $tambah_tanggal = mktime(0,0,0,date('m')+0,date('d')+0,date('Y')+1);

          $tanggalAktif = date('Y-m-d',$tambah_tanggal);

          if (!empty($lokasi_file)){
            UploadImage($nama_file_unik);
            $linkhosting	= 'http://localhost:8000/assets/gambar_artikel/small_'.$nama_file_unik;
          }else{
            $nama_file_unik = 'noimage.png';
            $linkhosting	= 'http://localhost:8000/assets/gambar_artikel/small_noimage.png';
          }

          if (!empty($lokasi_file2)){
            UploadImage2($nama_file_unik2);
            $linkhosting2	= 'http://localhost:8000/assets/gambar_artikel/small_'.$nama_file_unik2;
          }else{
            $nama_file_unik = 'noimage.png';
            $linkhosting2	= 'http://localhost:8000/assets/gambar_artikel/small_noimage.png';
          }

          $kode  = 'VIDEO-ARTIKEL';
          $index = DB::table('artikel_video')
                            ->orderBy('id','DESC')
                            ->get();

          $tglSekarang = date('Y-m-d');

          if (!isset($index[0])) {
              $newid = $kode.'00001';
          } else{
              $index = $index[0]->id + 1;
              $a     = str_pad($index, 4, "0", STR_PAD_LEFT);
              $newid = $kode.'-'.$tglSekarang.'-'.$a;
          }

          if($request->hasFile('video')){

            $file = $request->file('video');
            $filename = $file->getClientOriginalName();
            $path = public_path().'/assets/video_artikel/';
            $file->move($path, $filename);
          }else{
            $filename = '';
          }

          $register = ArtikelVideo::create([
            'id_artikel'        => $newid,
            'judul_id'          => $input['txtJudul'],
            // 'judul_en'          => $input['txtJudul2'],
            // 'gambar_artikel1'   => $nama_file_unik,
            // 'gambar_artikel2'   => $nama_file_unik2,
            // 'info_gambar1'      => $input['txtInfoGambar'],
            // 'info_gambar2'      => $input['txtInfoGambar2'],
            // 'video_artikel'     => $filename,
            'link_video'        => $input['link_video'],
            'isi_artikel_id1'   => $input['txtIsi'],
            // 'isi_artikel_id2'   => $input['txtIsi2'],
            // 'isi_artikel_en1'   => $input['txtIsi3'],
            // 'isi_artikel_en2'   => $input['txtIsi4'],
            'video_slug'        => Str::slug($input['txtJudul']),
            'keyword'           => $input['txtMeta'],
            'sumber_video'      => $input['txtSumber'],
            'slug1'             => Str::slug($input['txtJudul']),
            // 'slug2'             => Str::slug($input['txtJudul2']),
            'tagar'             => '',
            // 'id_kategori'       => $input['txtKategori'],
            // 'id_penulis'        => $input['txtPenulis'],
            'hitung_tayang'     => '0',
            'status_publish'    => '1',
            'id_editor_video'   => $input['txtEditor'],
            'id_user'           => Session::get('id'),
            'created_at'        => Carbon::now(),
            'updated_at'        => Carbon::now(),
          ]);
        }

        $msg = 'Video Artikel Berhasil di input';

        return redirect('artikel-video')->with('msg',$msg);



    }


    public function nonaktif($id)
      {
        $hasilCostumers=DB::table('costumers')->where('id', '=', $id)->get();
    //		  dd($hasilUsers);
        $pesanError = array();

        if (!isset($hasilCostumers[0])) {
            $pesanError[] = "Data tidak ada !";
        }

        if (count($pesanError)>=1 ){
            return redirect('admin/costumers')->with('pesanUpdateError',$pesanError);
        }else{

        if ($hasilCostumers[0]->status_block == 1){
            $hasilBlock = 0;
        }else{
            $hasilBlock = 1;
        }

          $data = ['status_block' => $hasilBlock,
  				 ];

  		DB::table('costumers')->where('id',$id)->update($data);

          $msg = 'Proses berhasil';

          return redirect('admin/costumers')->with('msg',$msg);;
        }
      }



    public function edit($id)
      {
        $title = "Admin | Artikel Video";
        $menubar = "artikelvideo";
        $active  = "video";

        $tampilArtikelVideo = DB::table('artikel_video')->where('id_artikel', '=', $id)->get();

        $pesanError = array();

        if (!isset($tampilArtikelVideo[0])) {
            $pesanError[] = "Data tidak ada !";
        }

        if (count($pesanError)>=1 ){
            return redirect('artikel-video-edit')->with('pesanUpdateError',$pesanError);
        }else{

        $tampilKategori = DB::table('kategori')
                                    ->get();
        $tampilEditor = DB::table('editor_video')
                                    ->get();

  		  $hasilId = $tampilArtikelVideo[0]->id_artikel;
  		  return view('backend.artikel_video.edit', ['title'=>$title,'menubar' => $menubar, 'active' => $active, 'tampilKategori'=>$tampilKategori, 'tampilEditor'=>$tampilEditor, 'tampilArtikelVideo' => $tampilArtikelVideo, 'hasilId' => $hasilId]);
        }
      }

    // Update costumer
    public function update(Request $request, $id)
    {
      $input = $request->all();

      $pesanError = array();
      if (trim($input['txtJudul'])=="") {
          $pesanError[] = "Judul tidak boleh kosong ";
      }
      if (trim($input['txtIsi'])=="") {
          $pesanError[] = "Isi tidak boleh kosong ";
      }

      if (trim($input['link_video'])=="") {
          $pesanError[] = "Link tidak boleh kosong ";
      }else{
          $linkVideo = $input['link_video'];
      }

      if (count($pesanError)>=1 ){
        $data = '';
        for($i=0;$i<count($pesanError);$i++){
          $data.=  $pesanError[$i].',';
        }
          return redirect('artikel-video-edit/'.$id)->with('pesanError', $data);
      }else{

        function UploadImage($fupload_name){
        //direktori gambar
        $vdir_upload = "assets/gambar_video_artikel/";
        $vfile_upload = $vdir_upload . $fupload_name;

        //Simpan gambar dalam ukuran sebenarnya
        move_uploaded_file($_FILES["gambar"]["tmp_name"], $vfile_upload);

          $typeUpload = $_FILES["gambar"]["type"];

          //identitas file asli
          if ($typeUpload == 'image/jpeg'){
          $im_src = imagecreatefromjpeg($vfile_upload);
          }else{
          $im_src = imagecreatefrompng($vfile_upload);
          }
        $src_width = imageSX($im_src);
        $src_height = imageSY($im_src);

        //Simpan dalam versi small 110 pixel
        //Set ukuran gambar hasil perubahan
        $dst_width = 580;
        $dst_height = ($dst_width/$src_width)*$src_height;

        //proses perubahan ukuran
        $im = imagecreatetruecolor($dst_width,$dst_height);
        imagecopyresampled($im, $im_src, 0, 0, 0, 0, $dst_width, $dst_height, $src_width, $src_height);

        //Simpan gambar
        imagejpeg($im,$vdir_upload . "small_" . $fupload_name);

        //Hapus gambar di memori komputer
        imagedestroy($im_src);
        imagedestroy($im);
        }

        // $lokasi_file    = $_FILES['gambar']['tmp_name'];
        // $tipe_file      = $_FILES['gambar']['type'];
        // $nama_file      = $_FILES['gambar']['name'];
        // $acak           = rand(1,999);
        // $nama_file_unik = $acak.$nama_file;

        $tambah_tanggal = mktime(0,0,0,date('m')+0,date('d')+0,date('Y')+1);

        $tanggalAktif = date('Y-m-d',$tambah_tanggal);

        if (!empty($lokasi_file)){
          if ($request->hidgambar!='noimage.png'){
          @unlink('assets/gambar_video_artikel/'.$request->hidgambar);
	        @unlink('assets/gambar_video_artikel/'.'small_'.$request->hidgambar);
          }
          UploadImage($nama_file_unik);
          $linkhosting	= 'http://127.0.0.1:8000/assets/gambar_video_artikel/small_'.$nama_file_unik;
        }else{
          $nama_file_unik = $request->hidgambar;
          $linkhosting	= 'http://127.0.0.1:8000/assets/gambar_video_artikel/small_'.$request->hidgambar;
        }

        if($request->hasFile('video')){

          $file = $request->file('video');
          $filename = $file->getClientOriginalName();
          $path = public_path().'/assets/video_artikel/';
          $file->move($path, $filename);

          @unlink('assets/video_artikel/'.$request->hidgambar);
        }else{
          $filename = $request->hidvideo;
        }

        $data = [
          'judul_id'          => $input['txtJudul'],
          // 'judul_en'          => $input['txtJudul2'],
          // 'gambar_artikel1'   => $nama_file_unik,
          // 'gambar_artikel2'   => $nama_file_unik2,
          // 'info_gambar1'      => $input['txtInfoGambar'],
          // 'info_gambar2'      => $input['txtInfoGambar2'],
          // 'video_artikel'     => $filename,
          'link_video'        => $input['link_video'],
          'isi_artikel_id1'   => $input['txtIsi'],
          // 'isi_artikel_id2'   => $input['txtIsi2'],
          // 'isi_artikel_en1'   => $input['txtIsi3'],
          // 'isi_artikel_en2'   => $input['txtIsi4'],
          'video_slug'        => Str::slug($input['txtJudul']),
          'keyword'           => $input['txtMeta'],
          'sumber_video'      => $input['txtSumber'],
          'slug1'             => Str::slug($input['txtJudul']),
          // 'slug2'             => Str::slug($input['txtJudul2']),
          'tagar'             => '',
          // 'id_kategori'       => $input['txtKategori'],
          // 'id_penulis'        => $input['txtPenulis'],
          'hitung_tayang'     => '0',
          'status_publish'    => '1',
          'id_editor_video'   => $input['txtEditor'],
          'id_user'           => Session::get('id'),
          'created_at'        => Carbon::now(),
          'updated_at'        => Carbon::now(),
				        ];
		DB::table('artikel_video')->where('id_artikel',$id)->update($data);

  }

    $msg = 'Artikel Video berhasil di perbaharui';

  return redirect('artikel-video')->with('msg',$msg);
  }

  public function update2(Request $request)
  {
    $input = $request->all();

    $hasilCostumers=DB::table('costumers')->where('id', '=', $request->id)->get();

    $hasilId = $hasilCostumers[0]->id;
    $hasilPin = $hasilCostumers[0]->pin;

    $pesanError2 = array();

    if($input['txtpinbaru']!=$input['txtpinbaruconfirm']){
        $pesanError2[] = "Data Pin Baru tidak sama !";
    }
    if (!is_numeric($input['txtpinbaru']) AND $input['txtpinbaru']!='') {
        $pesanError2[] = "Data Pin Baru tidak sama !";
    }
    if($input['txtpasswordbaru']!=$input['txtpasswordbaruconfirm']){
        $pesanError2[] = "Data Password Baru tidak sama !";
    }

    if (count($pesanError2)>=1 ){
        return redirect('admin/costumers/edit/'.$request->id)->with('pesanError2',$pesanError2);
    }else{

      if($input['txtpinbaru']==""){
          $pinBaru = '';
      }else{
          $pinBaru = $input['txtpinbaru'];
      }

      if($input['txtpasswordbaru']==""){
          $passwordBaru = '';
          $passwordBaruAndroid = '';
      }else{
          $passwordBaru = bcrypt($input['txtpasswordbaru']);
          $passwordBaruAndroid = md5($input['txtpasswordbaru']);
      }


      $data = ['password' 			=> $passwordBaru,
             'android_password' => $passwordBaruAndroid,
             'pin'          => $pinBaru,
             'updated_at' 	=> Carbon::now(),
       ];
  DB::table('costumers')->where('id',$request->id)->update($data);

}

if (isset($data)) {
  $msg = 'Sukses';
} else {
  $msg = 'Maaf Belum Berhasil';
}

return redirect('admin/costumers')->with('msg',$msg);
}
public function destroy($id)
{
  $hasilCostumers=DB::table('artikel_video')->where('id_artikel', '=', $id)->get();
  //dd($hasilUsers);
  $pesanError = array();

  if (!isset($hasilCostumers[0])) {
      $pesanError[] = "Data tidak ada !";
  }

  if (count($pesanError)>=1 ){
      return redirect('/artikel-video')->with('pesanUpdateError',$pesanError);
  }else{

    DB::table('artikel_video')->where('id_artikel', '=', $id)->delete();
    $users = DB::table('artikel_video')->simplePaginate(10);

    $msg = 'Artikel berhasil di hapus';

    return redirect('/artikel-video')->with('msg',$msg);;
  }
}

	// Verifikasi
    public function verifikasi(Request $request)
    {
      $hasilCostumers=DB::table('costumers')->where('id', '=', $request->id)->get();

//      $fromEmail = "admin@justusku.co.id";

//      $fromEmail = "justus@inovindoapps.com";

      $pesanError = array();

      if (!isset($hasilCostumers[0])) {
          $pesanError[] = "Data tidak ada !";
      }

      if (count($pesanError)>=1 ){
          return redirect('admin/costumers')->with('pesanUpdateError',$pesanError);
      }else{

        $data = ['password' => bcrypt($hasilCostumers[0]->hint),
                 'status_aktif'	=> '1',
                 'valid_until' => '1',
                ];

        $fromEmail = "admin@justusku.co.id";

        $headers = array("From: ".$fromEmail,
                "Reply-To: ".$fromEmail,
                "X-Mailer: PHP/" . PHP_VERSION,
                "Content-type: text/html; charset=iso-8859-1MIME-Version: 1.0"
        );


        $headers = implode("\r\n", $headers);

        $emailMessage = array(
                "Selamat member Anda sudah aktif anda dapat melakukan login ke aplikasi justus untuk mengetahui info terbaru dan penggunaan point anda serta keuntungan lainnya. <br><br>",
                "Email : ".$hasilCostumers[0]->email."<br>
                Password : ".$hasilCostumers[0]->hint."<br>
                Pin : ".$hasilCostumers[0]->pin."<br>"
                );
        $emailMessage = implode("\r\n", $emailMessage);

        $subject  = "Register";

        mail($hasilCostumers[0]->email, "$subject", $emailMessage, $headers, "-f".$fromEmail);

        $msg = 'Verifikasi Data Berhasil';

		DB::table('costumers')->where('id',$request->id)->update($data);
		return redirect('admin/costumers')->with('msg',$msg);
      }
    }

}
