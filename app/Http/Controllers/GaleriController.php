<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use Carbon\Carbon;
use Redirect;
use DataTables;
use Auth;
use Mail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Galeri;

class GaleriController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

      $title = 'Admin | Galeri';
      $menubar = "galeri";
      $active  = "";

      $tampilGaleri = DB::table('galeri_tokoh')
                        ->get();

      return view('backend.galeri.index', ['menubar' => $menubar, 'active' => $active, 'tampilGaleri' => $tampilGaleri, 'title' => $title]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Admin | Galeri';
        $menubar = "galeri";
        $active  = "";
        return view('backend.galeri.tambah', ['menubar' => $menubar, 'active' => $active, 'title' => $title]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $input = $request->all();

      $pesanError = array();
      if (trim($input['txtNama'])=="") {
          $pesanError[] = "Nama Tokoh tidak boleh kosong !";
      }
      if (trim($input['txtJabatan'])=="") {
          $pesanError[] = "Jabatan tidak boleh kosong !";
      }

      if (count($pesanError)>=1 ){
        $data = '';
        for($i=0;$i<count($pesanError);$i++){
          $data.=  $pesanError[$i].',';
        }
          return redirect('/galeri-tambah')->with('pesanError',$data);
      }else{

        function UploadImage($fupload_name){
        //direktori gambar
        $vdir_upload = "assets/foto_galeri/";
        $vfile_upload = $vdir_upload . $fupload_name;

        //Simpan gambar dalam ukuran sebenarnya
        move_uploaded_file($_FILES["gambar"]["tmp_name"], $vfile_upload);

        $typeUpload = $_FILES["gambar"]["type"];

        //identitas file asli
        if ($typeUpload == 'image/jpeg'){
        $im_src = imagecreatefromjpeg($vfile_upload);
        }else{
        $im_src = imagecreatefrompng($vfile_upload);
        }
        $src_width = imageSX($im_src);
        $src_height = imageSY($im_src);

        //Simpan dalam versi small 110 pixel
        //Set ukuran gambar hasil perubahan
        $dst_width = 580;
        $dst_height = ($dst_width/$src_width)*$src_height;

        //proses perubahan ukuran
        $im = imagecreatetruecolor($dst_width,$dst_height);
        imagecopyresampled($im, $im_src, 0, 0, 0, 0, $dst_width, $dst_height, $src_width, $src_height);

        //Simpan gambar
        imagejpeg($im,$vdir_upload . "small_" . $fupload_name);

        //Hapus gambar di memori komputer
        imagedestroy($im_src);
        imagedestroy($im);
        }

        $lokasi_file    = $_FILES['gambar']['tmp_name'];
        $tipe_file      = $_FILES['gambar']['type'];
        $nama_file      = $_FILES['gambar']['name'];
        $acak           = rand(1,999);
        $nama_file_unik = $acak.$nama_file;

        $tambah_tanggal = mktime(0,0,0,date('m')+0,date('d')+0,date('Y')+1);

        $tanggalAktif = date('Y-m-d',$tambah_tanggal);

        if (!empty($lokasi_file)){
          UploadImage($nama_file_unik);
          $linkhosting	= 'http://127.0.0.1:8000/assets/foto_galeri/small_'.$nama_file_unik;
        }else{
          $nama_file_unik = 'noimage.png';
          $linkhosting	= 'http://127.0.0.1:8000/assets/foto_galeri/small_noimage.png';
        }

        $register = Galeri::create([
          'nama_tokoh' => $input['txtNama'],
          'foto_tokoh' => $nama_file_unik,
          'jabatan_tokoh' => $input['txtJabatan'],
          'created_at' => Carbon::now(),
          'updated_at' => Carbon::now(),
        ]);
      }

      $msg = 'Data Galeri berhasil di tambahkan';

      return redirect('/galeri')->with('msg',$msg);


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title = 'Admin | Galeri';
        $menubar = "galeri";
        $active  = "";
        //Find the user object from model if it exists
        $tampilGaleri = DB::table('galeri_tokoh')->where('id_galeri', '=', $id)->get();
        //		  dd($hasilUsers);
        $pesanError = array();

        if (!isset($tampilGaleri[0])) {
          $pesanError[] = "Data tidak ada !";
        }

        if (count($pesanError)>=1 ){
          return redirect('/galeri-edit')->with('pesanUpdateError',$pesanError);
        }else{

        $hasilId = $tampilGaleri[0]->id_galeri;
          return view('backend.galeri.edit', ['menubar' => $menubar, 'active' => $active, 'tampilGaleri' => $tampilGaleri, 'hasilId' => $hasilId, 'title' => $title]);
        }
      }

      /**
       * Update the specified resource in storage.
       *
       * @param  \Illuminate\Http\Request  $request
       * @param  int  $id
       * @return \Illuminate\Http\Response
       */
      public function update(Request $request, $id)
      {
        $input = $request->all();

      $pesanError = array();
      if (trim($input['txtNama'])=="") {
          $pesanError[] = "Nama Tokoh tidak boleh kosong !";
      }
      $pesanError = array();
      if (trim($input['txtJabatan'])=="") {
          $pesanError[] = "Jabatan tidak boleh kosong !";
      }


      if (count($pesanError)>=1 ){
        $data = '';
        for($i=0;$i<count($pesanError);$i++){
          $data.=  $pesanError[$i].',';
        }
          return redirect('galeri-edit/'.$id)->with('pesanError',$data);
      }else{

        function UploadImage($fupload_name){
        //direktori gambar
        $vdir_upload = "assets/foto_galeri/";
        $vfile_upload = $vdir_upload . $fupload_name;

        //Simpan gambar dalam ukuran sebenarnya
        move_uploaded_file($_FILES["gambar"]["tmp_name"], $vfile_upload);

          $typeUpload = $_FILES["gambar"]["type"];

          //identitas file asli
          if ($typeUpload == 'image/jpeg'){
          $im_src = imagecreatefromjpeg($vfile_upload);
          }else{
          $im_src = imagecreatefrompng($vfile_upload);
          }
        $src_width = imageSX($im_src);
        $src_height = imageSY($im_src);

        //Simpan dalam versi small 110 pixel
        //Set ukuran gambar hasil perubahan
        $dst_width = 580;
        $dst_height = ($dst_width/$src_width)*$src_height;

        //proses perubahan ukuran
        $im = imagecreatetruecolor($dst_width,$dst_height);
        imagecopyresampled($im, $im_src, 0, 0, 0, 0, $dst_width, $dst_height, $src_width, $src_height);

        //Simpan gambar
        imagejpeg($im,$vdir_upload . "small_" . $fupload_name);

        //Hapus gambar di memori komputer
        imagedestroy($im_src);
        imagedestroy($im);
        }

        $lokasi_file    = $_FILES['gambar']['tmp_name'];
        $tipe_file      = $_FILES['gambar']['type'];
        $nama_file      = $_FILES['gambar']['name'];
        $acak           = rand(1,999);
        $nama_file_unik = $acak.$nama_file;

        $tambah_tanggal = mktime(0,0,0,date('m')+0,date('d')+0,date('Y')+1);

        $tanggalAktif = date('Y-m-d',$tambah_tanggal);

        if (!empty($lokasi_file)){
          if ($request->hidgambar!='noimage.png'){
          @unlink('assets/foto_galeri/'.$request->hidgambar);
          @unlink('assets/foto_galeri/'.'small_'.$request->hidgambar);
          }
          UploadImage($nama_file_unik);
          $linkhosting	= 'http://127.0.0.1:8000/assets/foto_galeri/small_'.$nama_file_unik;
        }else{
          $nama_file_unik = $request->hidgambar;
          $linkhosting	= 'http://127.0.0.1:8000/assets/foto_galeri/small_'.$request->hidgambar;
        }


        $data = ['nama_tokoh' => $input['txtNama'],
                 'foto_tokoh' => $nama_file_unik,
                 'jabatan_tokoh' => $input['txtJabatan'],
                 'created_at' => Carbon::now(),
                 'updated_at' => Carbon::now(),
                ];
    DB::table('galeri_tokoh')->where('id_galeri',$id)->update($data);

  }

    $msg = 'Data Galeri berhasil di perbaharui';

  return redirect('galeri')->with('msg',$msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $hasilCostumers=DB::table('galeri_tokoh')->where('id_galeri', '=', $id)->get();
      //dd($hasilUsers);
      $pesanError = array();

      if (!isset($hasilCostumers[0])) {
          $pesanError[] = "Data tidak ada !";
      }

      if (count($pesanError)>=1 ){
          return redirect('/galeri')->with('pesanUpdateError',$pesanError);
      }else{

        DB::table('galeri_tokoh')->where('id_galeri', '=', $id)->delete();
        $users = DB::table('galeri_tokoh')->simplePaginate(10);

        $msg = 'Data berhasil di hapus';

        return redirect('/galeri')->with('msg',$msg);;
      }
    }
}
