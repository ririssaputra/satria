<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Redirect;
use DataTables;
use Auth;
use Mail;
use View;

class FrontEndController extends Controller
{
    public function index()
    {
      $hasilArtikelTerpopuler = DB::table('artikel')
                                ->select('sub_kategori.*','artikel.*','penulis.*')
                                ->leftJoin('sub_kategori', 'sub_kategori.id_sub_kategori', '=', 'artikel.id_sub_kategori')
                                ->leftJoin('penulis', 'penulis.id_penulis', '=', 'artikel.id_penulis')
                                // ->orderBy('DESC')
                                ->get();
      $hasilVideoTerpopuler = DB::table('artikel_video')
                                // ->select('kategori.*','artikel.*','penulis.*')
                                // ->leftJoin('kategori', 'kategori.id_kategori', '=', 'artikel.id_kategori')
                                ->take(4)
                                ->orderBy('artikel_video.id', 'DESC')
                                ->get();
      return view ('frontend.beranda', ['hasilArtikelTerpopuler' => $hasilArtikelTerpopuler, 'hasilVideoTerpopuler' => $hasilVideoTerpopuler]);
    }

    public function detail_artikel($url, $id)
    {
      $hasilArtikelKesenian = DB::table('artikel')
                      ->select('artikel.*', 'penulis.*')
                      ->leftjoin('penulis', 'artikel.id_penulis', '=', 'penulis.id_penulis')
                      ->where('slug1', '=', $id)
                      // ->where('artikel.date_publish', '<=', Carbon::now())
                      ->get();

      // $hasilArtikelTranding = DB::table('artikel')
      //                 ->where('artikel.date_publish', '<=', Carbon::now())
      //                 ->orderBy('id', 'DESC')
      //                 ->take(8)
      //                 ->get();

      // $hasilArtikelPopuler = DB::table('artikel')
      //                 ->where('artikel.date_publish', '<=', Carbon::now())
      //                 ->orderBy('hitung_tayang', 'DESC')
      //                 ->take(5)
      //                 ->get();
                      
      // $hasilArtikelRandom = DB::table('artikel')
      //                 ->select('artikel.*','sub_kategori.*')
      //                 ->leftjoin('sub_kategori', 'sub_kategori.id_sub_kategori', '=', 'artikel.id_sub_kategori')
      //                 ->where('artikel.date_publish', '<=', Carbon::now())
      //                 ->orderBy(DB::raw('RAND()'))
      //                 ->take(5)
      //                 ->get();
                      
      // $hasilArtikelTerkait = DB::table('artikel')
      //                 ->where('id_kategori', '=', $hasilPlayVideo[0]->id_kategori)
      //                 ->where('artikel.date_publish', '<=', Carbon::now())
      //                 ->orderBy(DB::raw('RAND()'))
      //                 ->take(3)
      //                 ->get();

        if (isset(Auth::user()->user_id)) {
            $user_id = Auth::user()->user_id;
        }else{
            $user_id = '';
        }

        DB::statement("UPDATE artikel SET hitung_tayang = hitung_tayang + 1 WHERE slug1 = '$id'");

        $shareDetailArtikel = 'ok';

        return view('frontend.detail_artikel', ['shareDetailArtikel' => $shareDetailArtikel, 'hasilArtikelKesenian' => $hasilArtikelKesenian, 'user_id' => $user_id]);
    }

}
