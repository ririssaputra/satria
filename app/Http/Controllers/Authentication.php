<?php

namespace App\Http\Controllers;

use App\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class Authentication extends Controller
{
    public function index(){
      if(!session::get('login')){
        return redirect('/')->with('alert','Kamu Harus Login Terlebih Dahulu');
      }
      else{
        return view('user');
      }

    }
    public function login(){
      return view('authentication/login');
    }
    public function loginPost (Request $request){
      $email = $request->email;
      $password = $request->password;

      $data = Auth::where('email',$email)->first();
      if(!$data){
        return redirect('login')->with('alert','Email Tidak Terdaftar');
      }
      else
      {
        if(Hash::check($password,$data->password)){
          Session::put('username',$data->username);
          Session::put('id',$data->id);
          Session::put('level',$data->level);
          Session::put('email',$data->email);
          Session::put('nama_lengkap',$data->nama_lengkap);
          Session::put('login',TRUE);
          if(session::get('level') == 'admin'){
            return redirect('admin');
          }
          elseif(session::get('level') == 'user'){
            return redirect('/');
          }
        }
        else
        {
          return redirect('login')->with('alert','Password atau Email, Salah !');
        }
      }
    }
    public function logout(){
        Session::flush();
        return redirect('login')->with('alert','Kamu sudah logout');
    }
    public function register(Request $request){
        return view('authentication/register');
    }
    public function registerPost(Request $request){
        $this->validate($request, [
            'username' => 'required',
            'nama_lengkap' => 'required',
            'email' => 'required|email|unique:t_user',
            'password' => 'required',
            'confirmation' => 'required|same:password',
        ]);

        $data =  new Auth();
        $data->username = $request->username;
        $data->email = $request->email;
        $data->nama_lengkap = $request->nama_lengkap;
        $data->level = $request->level;
        $data->password = bcrypt($request->password);
        $data->save();
        return redirect('/')->with('alert-success','Kamu berhasil Register');
    }
}
