<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use Carbon\Carbon;
use Redirect;
use DataTables;
use Auth;
use Mail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Editor;

class EditorController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

      $title = 'Admin | Editor Video';
      $menubar = "masterdata";
      $active  = "master";

      $tampilEditor = DB::table('editor_video')
                        ->get();

      return view('backend.editor_video.index', ['menubar' => $menubar, 'active' => $active, 'tampilEditor' => $tampilEditor, 'title' => $title]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Admin | Editor Video';
        $menubar = "masterdata";
        $active  = "master";

        return view('backend.editor_video.tambah', ['menubar' => $menubar, 'active' => $active, 'title' => $title]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $input = $request->all();

      $pesanError = array();
      if (trim($input['txtNamaEditor'])=="") {
          $pesanError[] = "Nama Editor tidak boleh kosong !";
      }
      if (trim($input['txtDeskripsi'])=="") {
          $pesanError[] = "Dekripsi Editor tidak boleh kosong !";
      }


      if (count($pesanError)>=1 ){
        $data = '';
        for($i=0;$i<count($pesanError);$i++){
          $data.=  $pesanError[$i].',';
        }
          return redirect('/editor-tambah')->with('pesanError',$data);
      }else{

        function UploadImage($fupload_name){
        //direktori gambar
        $vdir_upload = "assets/foto_editor/";
        $vfile_upload = $vdir_upload . $fupload_name;

        //Simpan gambar dalam ukuran sebenarnya
        move_uploaded_file($_FILES["gambar"]["tmp_name"], $vfile_upload);

        $typeUpload = $_FILES["gambar"]["type"];

        //identitas file asli
        if ($typeUpload == 'image/jpeg'){
        $im_src = imagecreatefromjpeg($vfile_upload);
        }else{
        $im_src = imagecreatefrompng($vfile_upload);
        }
        $src_width = imageSX($im_src);
        $src_height = imageSY($im_src);

        //Simpan dalam versi small 110 pixel
        //Set ukuran gambar hasil perubahan
        $dst_width = 580;
        $dst_height = ($dst_width/$src_width)*$src_height;

        //proses perubahan ukuran
        $im = imagecreatetruecolor($dst_width,$dst_height);
        imagecopyresampled($im, $im_src, 0, 0, 0, 0, $dst_width, $dst_height, $src_width, $src_height);

        //Simpan gambar
        imagejpeg($im,$vdir_upload . "small_" . $fupload_name);

        //Hapus gambar di memori komputer
        imagedestroy($im_src);
        imagedestroy($im);
        }

        $lokasi_file    = $_FILES['gambar']['tmp_name'];
        $tipe_file      = $_FILES['gambar']['type'];
        $nama_file      = $_FILES['gambar']['name'];
        $acak           = rand(1,999);
        $nama_file_unik = $acak.$nama_file;

        $tambah_tanggal = mktime(0,0,0,date('m')+0,date('d')+0,date('Y')+1);

        $tanggalAktif = date('Y-m-d',$tambah_tanggal);

        if (!empty($lokasi_file)){
          UploadImage($nama_file_unik);
          $linkhosting	= 'http://127.0.0.1:8000/assets/foto_editor/small_'.$nama_file_unik;
        }else{
          $nama_file_unik = 'noimage.png';
          $linkhosting	= 'http://127.0.0.1:8000/assets/foto_editor/small_noimage.png';
        }

        // $kode  = 'ARTIKEL';
        // $index = DB::table('artikel')
        //                   ->orderBy('id','DESC')
        //                   ->get();
        //
        // $tglSekarang = date('Y-m-d');
        //
        // if (!isset($index[0])) {
        //     $newid = $kode.'00001';
        // } else{
        //     $index = $index[0]->id + 1;
        //     $a     = str_pad($index, 4, "0", STR_PAD_LEFT);
        //     $newid = $kode.'-'.$tglSekarang.'-'.$a;
        // }

        $register = Editor::create([
          'nama_editor' => $input['txtNamaEditor'],
          'foto_editor' => $nama_file_unik,
          'deskripsi_editor' => $input['txtDeskripsi'],
          // 'status' => '0',
          'created_at' => Carbon::now(),
          'updated_at' => Carbon::now(),
        ]);
      }

      $msg = 'Data berhasil di tambahkan';

      return redirect('/editor')->with('msg',$msg);


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title = 'Admin | Editor Video';
        $menubar = "masterdata";
        $active  = "master";
        //Find the user object from model if it exists
        $tampilEditor = DB::table('editor_video')->where('id_editor_video', '=', $id)->get();
        //		  dd($hasilUsers);
        $pesanError = array();

        if (!isset($tampilEditor[0])) {
          $pesanError[] = "Data tidak ada !";
        }

        if (count($pesanError)>=1 ){
          return redirect('/editor-edit')->with('pesanUpdateError',$pesanError);
        }else{

        $hasilId = $tampilEditor[0]->id_editor_video;
        return view('backend.editor_video.edit', ['menubar' => $menubar, 'active' => $active, 'tampilEditor' => $tampilEditor, 'hasilId' => $hasilId, 'title' => $title]);
        }
      }

      /**
       * Update the specified resource in storage.
       *
       * @param  \Illuminate\Http\Request  $request
       * @param  int  $id
       * @return \Illuminate\Http\Response
       */
      public function update(Request $request, $id)
      {
        $input = $request->all();

      $pesanError = array();
      if (trim($input['txtNamaEditor'])=="") {
          $pesanError[] = "Nama Editor tidak boleh kosong !";
      }
      $pesanError = array();
      if (trim($input['txtDeskripsi'])=="") {
          $pesanError[] = "Deskripsi tidak boleh kosong !";
      }


      if (count($pesanError)>=1 ){
          return redirect('editor-edit/'.$id)->with('pesanError',$pesanError);
      }else{

        function UploadImage($fupload_name){
        //direktori gambar
        $vdir_upload = "assets/foto_editor/";
        $vfile_upload = $vdir_upload . $fupload_name;

        //Simpan gambar dalam ukuran sebenarnya
        move_uploaded_file($_FILES["gambar"]["tmp_name"], $vfile_upload);

          $typeUpload = $_FILES["gambar"]["type"];

          //identitas file asli
          if ($typeUpload == 'image/jpeg'){
          $im_src = imagecreatefromjpeg($vfile_upload);
          }else{
          $im_src = imagecreatefrompng($vfile_upload);
          }
        $src_width = imageSX($im_src);
        $src_height = imageSY($im_src);

        //Simpan dalam versi small 110 pixel
        //Set ukuran gambar hasil perubahan
        $dst_width = 580;
        $dst_height = ($dst_width/$src_width)*$src_height;

        //proses perubahan ukuran
        $im = imagecreatetruecolor($dst_width,$dst_height);
        imagecopyresampled($im, $im_src, 0, 0, 0, 0, $dst_width, $dst_height, $src_width, $src_height);

        //Simpan gambar
        imagejpeg($im,$vdir_upload . "small_" . $fupload_name);

        //Hapus gambar di memori komputer
        imagedestroy($im_src);
        imagedestroy($im);
        }

        $lokasi_file    = $_FILES['gambar']['tmp_name'];
        $tipe_file      = $_FILES['gambar']['type'];
        $nama_file      = $_FILES['gambar']['name'];
        $acak           = rand(1,999);
        $nama_file_unik = $acak.$nama_file;

        $tambah_tanggal = mktime(0,0,0,date('m')+0,date('d')+0,date('Y')+1);

        $tanggalAktif = date('Y-m-d',$tambah_tanggal);

        if (!empty($lokasi_file)){
          if ($request->hidgambar!='noimage.png'){
          @unlink('assets/foto_editor/'.$request->hidgambar);
          @unlink('assets/foto_editor/'.'small_'.$request->hidgambar);
          }
          UploadImage($nama_file_unik);
          $linkhosting	= 'http://127.0.0.1:8000/assets/foto_editor/small_'.$nama_file_unik;
        }else{
          $nama_file_unik = $request->hidgambar;
          $linkhosting	= 'http://127.0.0.1:8000/assets/foto_editor/small_'.$request->hidgambar;
        }


        $data = ['nama_editor' => $input['txtNamaEditor'],
                 'foto_editor' => $nama_file_unik,
                 'deskripsi_editor' => $input['txtDeskripsi'],
                 'created_at' => Carbon::now(),
                 'updated_at' => Carbon::now(),
                ];
    DB::table('editor_video')->where('id_editor_video',$id)->update($data);

  }

    $msg = 'Data berhasil di perbaharui';

  return redirect('editor')->with('msg',$msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $hasilCostumers=DB::table('editor_video')->where('id_editor_video', '=', $id)->get();
      //dd($hasilUsers);
      $pesanError = array();

      if (!isset($hasilCostumers[0])) {
          $pesanError[] = "Data tidak ada !";
      }

      if (count($pesanError)>=1 ){
          return redirect('/editor')->with('pesanUpdateError',$pesanError);
      }else{

        DB::table('editor_video')->where('id_editor_video', '=', $id)->delete();
        $users = DB::table('editor_video')->simplePaginate(10);

        $msg = 'Data berhasil di hapus';

        return redirect('/editor')->with('msg',$msg);;
      }
    }
}
