<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Artikel extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $table = 'artikel';

    protected $fillable = [
        'id',
        'id_artikel',
        'judul_id',
        'judul_en',
        'gambar_artikel1',
        'gambar_artikel2',
    		'info_gambar1',
    		'info_gambar2',
        'isi_artikel_id1',
        'isi_artikel_id2',
        'isi_artikel_en1',
        'isi_artikel_en2',
        'keyword',
        'slug1',
        'slug2',
        'hitung_tayang',
        'status_publish',
        'id_kategori',
        'id_penulis',
        'id_user',
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    protected $hidden = [
        'id',
    ];
}
