<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Penulis extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $table = 'penulis';

    protected $fillable = [
        'id_penulis',
        'nama_penulis',
        'foto_penulis',
        'deskripsi_penulis',
        'updated_at',
        'created_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    protected $hidden = [
        'id_penulis',
    ];
}
