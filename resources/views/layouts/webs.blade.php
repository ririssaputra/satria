
<!doctype html>
<html lang="en" class="no-js">

<!-- Mirrored from nunforest.com/hotmagazine/default/home6.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 23 Jun 2020 09:37:55 GMT -->
<head>
	<title>Sunda Sakti</title>

	<meta charset="utf-8">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900,400italic' rel='stylesheet' type='text/css'>
	<link href="{{ asset('hot/maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css') }}" rel="stylesheet">
  <link rel="icon" type="image/x-icon" href="{{ asset('img/logo/logo-ss.jpg') }}"/>
	<link rel="stylesheet" type="text/css" href="{{ asset('hot/nunforest.com/hotmagazine/default/css/bootstrap.min.css') }}" media="screen">
	<link rel="stylesheet" type="text/css" href="{{ asset('hot/nunforest.com/hotmagazine/default/css/jquery.bxslider.css') }}" media="screen">
	<link rel="stylesheet" type="text/css" href="{{ asset('hot/nunforest.com/hotmagazine/default/css/font-awesome.css') }}" media="screen">
	<link rel="stylesheet" type="text/css" href="{{ asset('hot/nunforest.com/hotmagazine/default/css/magnific-popup.css') }}" media="screen">
	<link rel="stylesheet" type="text/css" href="{{ asset('hot/nunforest.com/hotmagazine/default/css/owl.carousel.css') }}" media="screen">
    <link rel="stylesheet" type="text/css" href="{{ asset('hot/nunforest.com/hotmagazine/default/css/owl.theme.css') }}" media="screen">
	<link rel="stylesheet" type="text/css" href="{{ asset('hot/nunforest.com/hotmagazine/default/css/ticker-style.css') }}"/>
	<link rel="stylesheet" type="text/css" href="{{ asset('hot/nunforest.com/hotmagazine/default/css/style.css') }}" media="screen">

</head>
<body>

	<!-- Container -->
	<div id="container">

		<!-- Header
		    ================================================== -->
		@include('layout.partials.header')
		<!-- End Header -->

		<!-- heading-news4-section
			================================================== -->
		@yield('content')

		
		<!-- footer
			================================================== -->
	@include('layout.partials.footer')
		<!-- End footer -->
   
	</div>
	<!-- End Container -->

	<script type="text/javascript" src="{{ asset('hot/nunforest.com/hotmagazine/default/js/jquery.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('hot/nunforest.com/hotmagazine/default/js/jquery.migrate.js') }}"></script>
	<script type="text/javascript" src="{{ asset('hot/nunforest.com/hotmagazine/default/js/jquery.bxslider.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('hot/nunforest.com/hotmagazine/default/js/jquery.magnific-popup.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('hot/nunforest.com/hotmagazine/default/js/bootstrap.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('hot/nunforest.com/hotmagazine/default/js/jquery.ticker.js') }}"></script>
	<script type="text/javascript" src="{{ asset('hot/nunforest.com/hotmagazine/default/js/jquery.imagesloaded.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('hot/nunforest.com/hotmagazine/default/js/jquery.isotope.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('hot/nunforest.com/hotmagazine/default/js/owl.carousel.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('hot/nunforest.com/hotmagazine/default/js/theia-sticky-sidebar.html') }}"></script>
	<script type="text/javascript" src="{{ asset('hot/nunforest.com/hotmagazine/default/js/sticky.html') }}"></script>
	<script type="text/javascript" src="{{ asset('hot/nunforest.com/hotmagazine/default/js/retina-1.1.0.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('hot/nunforest.com/hotmagazine/default/js/plugins-scroll.js') }}"></script>
	<script type="text/javascript" src="{{ asset('hot/nunforest.com/hotmagazine/default/js/script.js') }}"></script>

</body>

<!-- Mirrored from nunforest.com/hotmagazine/default/home6.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 23 Jun 2020 09:38:15 GMT -->
</html>
