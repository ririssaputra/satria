@extends('layouts.webs')
@section('content')
<section class="block-wrapper">
	<div class="container">
		<div class="row">
			<div class="col-sm-8">

				<!-- block content -->
				<div class="block-content">

					<!-- single-post box -->
					<div class="single-post-box">

						<div class="title-post">
							<h1>{{ $hasilArtikelKesenian[0]->judul_id }}</h1>
							<ul class="post-tags">
								<li><i class="fa fa-clock-o"></i>{{ \Carbon\Carbon::parse($hasilArtikelKesenian[0]->created_at)->format('d/m/Y H:i:s') }}</li>
								<li><i class="fa fa-user"></i>by <a href="#">{{ $hasilArtikelKesenian[0]->nama_penulis }}</a></li>
								<!-- <li><a href="#"><i class="fa fa-comments-o"></i><span>0</span></a></li> -->
								<li><i class="fa fa-eye"></i>@currency($hasilArtikelKesenian[0]->hitung_tayang)</li>
							</ul>
						</div>

						<div class="share-post-box">
							<ul class="share-box">
								<li><i class="fa fa-share-alt"></i><span>Bagikan :</span></li>
								<li><a class="facebook" href="javascript:void(0);" onclick="fb_share('{{ $hasilArtikelKesenian[0]->judul_id }}','https://{{ $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']}}')"><i class="fa fa-facebook"></i><span>Facebook</span></a></li>
								<li><a class="twitter" href="#"><i class="fa fa-twitter"></i><span>Twitter</span></a></li>
							</ul>
						</div>

						<div class="post-gallery">
							<img src="/assets/gambar_artikel/{{ $hasilArtikelKesenian[0]->gambar_artikel1 }}" alt="{{ $hasilArtikelKesenian[0]->info_gambar1 }}">
							<span class="image-caption">{{ $hasilArtikelKesenian[0]->info_gambar1 }}</span>
						</div>

						<div class="post-content">

							<p>{!! $hasilArtikelKesenian[0]->isi_artikel_id1 !!}</p>

							<p>{!! $hasilArtikelKesenian[0]->isi_artikel_en1 !!}</p>
							<!-- <blockquote>
								<p>Morbi interdum mollis sapien. Sed ac risus. Phasellus lacinia, magna a ullamcorper laoreet, lectus arcu pulvinar risus, vitae facilisis libero dolor a purus. Sed vel lacus. Mauris nibh felis, adipiscing varius, adipiscing in, lacinia vel, tellus. </p>
							</blockquote> -->
						</div>

						<div class="post-gallery">
							<img src="/assets/gambar_artikel/{{ $hasilArtikelKesenian[0]->gambar_artikel2 }}" alt="{{ $hasilArtikelKesenian[0]->info_gambar2 }}">
							<span class="image-caption">{{ $hasilArtikelKesenian[0]->info_gambar2 }}</span>
						</div>

						<div class="post-content">

							<p>{!! $hasilArtikelKesenian[0]->isi_artikel_id2 !!}</p>

							<p>{!! $hasilArtikelKesenian[0]->isi_artikel_en2 !!}</p>

						</div>

						<!-- <div class="article-inpost">
							<div class="row">
								<div class="col-md-6">
									<div class="image-content">
										<div class="image-place">
											<img src="{{ asset('hot/nunforest.com/hotmagazine/default/upload/news-posts/single-art.jpg') }}" alt="">
											<div class="hover-image">
												<a class="zoom" href="{{ asset('hot/nunforest.com/hotmagazine/default/upload/news-posts/single-art.jpg') }}">
													<i class="fa fa-arrows-alt"></i>
												</a>
											</div>
										</div>
										<span class="image-caption">Cras eget sem nec dui volutpat ultrices.</span>
									</div>
								</div>
								<div class="col-md-6">
									<div class="text-content">
										<h2>Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. </h2>
										<p>Suspendisse mauris. Fusce accumsan mollis eros. Pellentesque a diam sit amet mi ullamcorper vehicula. Integer adipiscing risus a sem. Nullam quis massa sit amet nibh viverra malesuada. </p>
										<p>Nunc sem lacus, accumsan quis, faucibus non, congue vel, arcu. Ut scelerisque hendrerit tellus. Integer sagittis. Vivamus a mauris eget arcu gravida tristique. </p>
										<p>Nunc iaculis mi in ante. Vivamus nibh feugiat est.</p>
									</div>
								</div>
							</div>
						</div> -->

						<div class="post-tags-box">
							<ul class="tags-box">
								<li><i class="fa fa-tags"></i><span>Tags:</span></li>
								<li><a href="#">News</a></li>
								<li><a href="#">Fashion</a></li>
								<li><a href="#">Politics</a></li>
								<li><a href="#">Sport</a></li>
							</ul>
						</div>

						<div class="prev-next-posts">
							<div class="prev-post">
								<img src="{{ asset('hot/nunforest.com/hotmagazine/default/upload/news-posts/listw4.jpg') }}" alt="">
								<div class="post-content">
									<h2><a href="single-post.html" title="prev post">Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </a></h2>
									<ul class="post-tags">
										<li><i class="fa fa-clock-o"></i>27 may 2013</li>
										<li><a href="#"><i class="fa fa-comments-o"></i><span>11</span></a></li>
									</ul>
								</div>
							</div>
							<div class="next-post">
								<img src="{{ asset('hot/nunforest.com/hotmagazine/default/upload/news-posts/listw5.jpg') }}" alt="">
								<div class="post-content">
									<h2><a href="single-post.html" title="next post">Donec consectetuer ligula vulputate sem tristique cursus. </a></h2>
									<ul class="post-tags">
										<li><i class="fa fa-clock-o"></i>27 may 2013</li>
										<li><a href="#"><i class="fa fa-comments-o"></i><span>8</span></a></li>
									</ul>
								</div>
							</div>
						</div>

						<div class="about-more-autor">
							<ul class="nav nav-tabs">
								<li class="active">
									<a href="#about-autor" data-toggle="tab">Tentang Penulis</a>
								</li>
								<!-- <li>
									<a href="#more-autor" data-toggle="tab">More From Autor</a>
								</li> -->
							</ul>

							<div class="tab-content">

								<div class="tab-pane active" id="about-autor">
									<div class="autor-box">
										<img src="/assets/foto_penulis/{{ $hasilArtikelKesenian[0]->foto_penulis }}" alt="{{ $hasilArtikelKesenian[0]->nama_penulis }}">
										<div class="autor-content">
											<div class="autor-title">
												<h1>{{ $hasilArtikelKesenian[0]->nama_penulis }}</h1>
												<!-- <ul class="autor-social">
													<li><a href="#" class="facebook"><i class="fa fa-facebook"></i></a></li>
													<li><a href="#" class="google"><i class="fa fa-google-plus"></i></a></li>
													<li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
													<li><a href="#" class="youtube"><i class="fa fa-youtube"></i></a></li>
													<li><a href="#" class="instagram"><i class="fa fa-instagram"></i></a></li>
													<li><a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
													<li><a href="#" class="dribble"><i class="fa fa-dribbble"></i></a></li>
												</ul> -->
											</div>
											<p>
												{!! $hasilArtikelKesenian[0]->deskripsi_penulis !!}
											</p>
										</div>
									</div>
								</div>

							<!-- 	<div class="tab-pane" id="more-autor">
									<div class="more-autor-posts">

										<div class="news-post image-post3">
											<img src="{{ asset('hot/nunforest.com/hotmagazine/default/upload/news-posts/gal1.jpg') }}" alt="">
											<div class="hover-box">
												<h2><a href="single-post.html">Donec odio. Quisque volutpat mattis eros.</a></h2>
												<ul class="post-tags">
													<li><i class="fa fa-clock-o"></i>27 may 2013</li>
												</ul>
											</div>
										</div>

										<div class="news-post image-post3">
											<img src="{{ asset('hot/nunforest.com/hotmagazine/default/upload/news-posts/gal2.jpg') }}" alt="">
											<div class="hover-box">
												<h2><a href="single-post.html">Nullam malesuada erat ut turpis. </a></h2>
												<ul class="post-tags">
													<li><i class="fa fa-clock-o"></i>27 may 2013</li>
												</ul>
											</div>
										</div>

										<div class="news-post image-post3">
											<img src="{{ asset('hot/nunforest.com/hotmagazine/default/upload/news-posts/gal3.jpg') }}" alt="">
											<div class="hover-box">
												<h2><a href="single-post.html">Suspendisse urna nibh.</a></h2>
												<ul class="post-tags">
													<li><i class="fa fa-clock-o"></i>27 may 2013</li>
												</ul>
											</div>
										</div>

										<div class="news-post image-post3">
											<img src="{{ asset('hot/nunforest.com/hotmagazine/default/upload/news-posts/gal4.jpg') }}" alt="">
											<div class="hover-box">
												<h2><a href="single-post.html">Donec nec justo eget felis facilisis fermentum. Aliquam </a></h2>
												<ul class="post-tags">
													<li><i class="fa fa-clock-o"></i>27 may 2013</li>
												</ul>
											</div>
										</div>

									</div>
								</div> -->

							</div>
						</div>

						<!-- carousel box -->
						<div class="carousel-box owl-wrapper">
							<div class="title-section">
								<h1><span>Artikel Lainnya</span></h1>
							</div>
							<div class="owl-carousel" data-num="3">
							
								<div class="item news-post image-post3">
									<img src="{{ asset('hot/nunforest.com/hotmagazine/default/upload/news-posts/art1.jpg') }}" alt="">
									<div class="hover-box">
										<h2><a href="single-post.html">Donec odio. Quisque volutpat mattis eros.</a></h2>
										<ul class="post-tags">
											<li><i class="fa fa-clock-o"></i>27 may 2013</li>
										</ul>
									</div>
								</div>
							
								<div class="item news-post image-post3">
									<img src="{{ asset('hot/nunforest.com/hotmagazine/default/upload/news-posts/art2.jpg') }}" alt="">
									<div class="hover-box">
										<h2><a href="single-post.html">Nullam malesuada erat ut turpis. </a></h2>
										<ul class="post-tags">
											<li><i class="fa fa-clock-o"></i>27 may 2013</li>
										</ul>
									</div>
								</div>
							
								<div class="item news-post video-post">
									<img src="{{ asset('hot/nunforest.com/hotmagazine/default/upload/news-posts/art3.jpg') }}" alt="">
									<a href="https://www.youtube.com/watch?v=LL59es7iy8Q" class="video-link"><i class="fa fa-play-circle-o"></i></a>
									<div class="hover-box">
										<h2><a href="single-post.html">Lorem ipsum dolor sit consectetuer adipiscing elit. Donec odio. </a></h2>
										<ul class="post-tags">
											<li><i class="fa fa-clock-o"></i>27 may 2013</li>
										</ul>
									</div>
								</div>
							
								<div class="item news-post image-post3">
									<img src="{{ asset('hot/nunforest.com/hotmagazine/default/upload/news-posts/art4.jpg') }}" alt="">
									<div class="hover-box">
										<h2><a href="single-post.html">Donec nec justo eget felis facilisis fermentum. Aliquam </a></h2>
										<ul class="post-tags">
											<li><i class="fa fa-clock-o"></i>27 may 2013</li>
										</ul>
									</div>
								</div>
							
								<div class="item news-post image-post3">
									<img src="{{ asset('hot/nunforest.com/hotmagazine/default/upload/news-posts/art5.jpg') }}" alt="">
									<div class="hover-box">
										<h2><a href="single-post.html">Donec odio. Quisque volutpat mattis eros.</a></h2>
										<ul class="post-tags">
											<li><i class="fa fa-clock-o"></i>27 may 2013</li>
										</ul>
									</div>
								</div>

							</div>
						</div>
						<!-- End carousel box -->

						<!-- contact form box -->
						<div class="contact-form-box">
							<div class="title-section">
								<h1><span>Leave a Comment</span> <span class="email-not-published">Your email address will not be published.</span></h1>
							</div>
							<form id="comment-form">
								<div class="row">
									<div class="col-md-4">
										<label for="name">Name*</label>
										<input id="name" name="name" type="text">
									</div>
									<div class="col-md-4">
										<label for="mail">E-mail*</label>
										<input id="mail" name="mail" type="text">
									</div>
									<div class="col-md-4">
										<label for="website">Website</label>
										<input id="website" name="website" type="text">
									</div>
								</div>
								<label for="comment">Comment*</label>
								<textarea id="comment" name="comment"></textarea>
								<button type="submit" id="submit-contact">
									<i class="fa fa-comment"></i> Post Comment
								</button>
							</form>
						</div>
						<!-- End contact form box -->

					</div>
					<!-- End single-post box -->

				</div>
				<!-- End block content -->

			</div>

		<div class="col-sm-4 sidebar-sticky">

				<!-- sidebar -->
			@include('layout.partials.sidebar_kanan')
				<!-- End sidebar -->

			</div>


		</div>

	</div>
</section>
<script type="text/javascript">
function fb_share(e,o){
  return u=o,t=e,window.open("https://www.facebook.com/sharer.php?u="+encodeURIComponent(u)+"&t="+encodeURIComponent(t),"sharer","toolbar=0,status=0,width=626,height=436"),!1}
  function tweet_share(t){return u=t,window.open("https://twitter.com/intent/tweet?text="+encodeURIComponent(u),"sharer","toolbar=0,status=0,width=626,height=436"),!1}
  function plus_share(t){return u=t,window.open("https://plus.google.com/share?url="+encodeURIComponent(u),"sharer","toolbar=0,status=0,width=626,height=436"),!1}
</script>
@endsection