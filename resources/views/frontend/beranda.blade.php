@extends('layouts.webs')
@section('content')
<section class="heading-news4">
	<div class="container">

		<!-- <div class="ticker-news-box">
			<span class="breaking-news">breaking news</span>
			<ul id="js-news">
				<li class="news-item"><span class="time-news">11:36 pm</span>  <a href="#">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</a> Donec odio. Quisque volutpat mattis eros... </li>
				<li class="news-item"><span class="time-news">12:40 pm</span>  <a href="#">Dëshmitarja Abrashi: E kam parë Oliverin në turmë,</a> ndërsa neve na shpëtoi “çika Mille” </li>
				<li class="news-item"><span class="time-news">11:36 pm</span>  <a href="#">Franca do të bashkëpunojë me Kosovën në fushën e shëndetësisë. </a></li>
				<li class="news-item"><span class="time-news">01:00 am</span>  <a href="#">DioGuardi, kështu e mbrojti Kosovën në Washington, </a> para serbit Vejvoda </li>
			</ul>
		</div> -->
	</div>

	<div class="heading-news-box">

		<div class="owl-wrapper">
			<div class="owl-carousel" data-num="4">

				<div class="item">
					<div class="news-post image-post4">
						<!-- <div class="post-gallery">
							<img src="{{ asset('hot/nunforest.com/hotmagazine/default/upload/news-posts/ap1.jpg') }}" alt="">
							<a class="category-post world" href="world.html">business</a>
						</div> -->
						<div class="post-content">
							<h2>Satria sunda sakti</h2>
								 <h4 style="color: white; font-family: verdana;">adalah komunitas lintas perguruan silat yang bertujuan  menjadikan sentral komunitas  Paguyuban silat di Bandung maupun di Jawa Barat umumnya.<br>
								 &nbsp;&nbsp;&nbsp;&nbsp;Paguyuban ini diharapkan dapat mengakomo-dir keinginan dari banyaknya Paguyuban silat maupun para pelaku seni silat yang ada di propinsi jawa barat khususnya bagian selatan (Aliran Pakidulan) dan  umumnya seluruh wilayah di  Jawa barat.<br>
								 &nbsp;&nbsp;&nbsp;&nbsp;Karena Paguyuban Satria Sunda Sakti adalah komunitas perguruan silat yang bersifat inklusif dan jauh dari sifat eklusifitas.</h4>
							<!-- <ul class="post-tags">
								<li><i class="fa fa-clock-o"></i>27 may 2013</li>
								<li><i class="fa fa-user"></i>by <a href="#">John Doe</a></li>
								<li><a href="#"><i class="fa fa-comments-o"></i><span>23</span></a></li>
								<li><i class="fa fa-eye"></i>872</li>
							</ul> -->
						</div>
					</div>
				</div>

				<div class="item">
					<div class="news-post image-post4">
						<div class="post-content">
							<h2>Visi</h2>
								 <h4 style="color: white; font-family: verdana;">menjadi wadah pusat unggulan kreativitas kesenian dan kebudayaan,mengembangkan dan membina seni kreatif yang sudah tertutupi arus moderenisasi.</h4>
						</div>
					</div>
				</div>

				<div class="item">
					<div class="news-post image-post4">
						<div class="post-content">
							<h2>Misi</h2>
								<ul>
								  <li style="color: white;">
										<h4 style="color: white; font-family: verdana;">Bertujuan menyatukan visi orang sunda di dalam memandang kompleksitas masalah berbangsa dan bernegara dengan konsep pemahaman dan intelektualitas sebagai orang sunda yang someah, egaliter, militant, silih asah, silih asih, silih asuh, silih riksa, silih jaga, silih wangun, silih hadean, silih wangikeun, sesuai yang di amanatkan oleh leluhur tatar sunda yaitu:</h4>
										<br><h4 style="color: white; font-style: italic; ">“Pamanah Rasa Sri Baduga Maharadja Ratu Hadji Di Pakuan Padjajaran Nya Dia Singaran Eyang Prabu Siliwangi”.</h4>
									</li>
								</ul>
								</div>
							</div>
						</div>
					<div class="item">
						<div class="news-post image-post4">
							<div class="post-content">
								<ul>
									<li style="color: white;">
										<h4 style="color: white; font-family: verdana;">Satria Sunda Sakti, ialah pelaku yang memiliki orientasi menyeimbangkan bahwa ketika Akhirat menjadi tujuan maka jangan lupakan nasib kita di dunia itulah yang Tuhan informasikan dalam Al-Quran.</h4>
									</li>
									<li style="color: white;">
										<h4 style="color: white; font-family: verdana;">Agama dan budaya tak bisa terpisahakan keduanya menjadi satu kesatuan, agama tak akan berkembang tanpa kebudayaan, begitupun kebudayaan akan serampangan tanpa nilai-nilai agama.</h4>
									</li>
								</ul>
						</div>
					</div>
				</div>

				<!-- <div class="item">
					<div class="news-post image-post4">
						<div class="post-gallery">
							<img src="{{ asset('hot/nunforest.com/hotmagazine/default/upload/news-posts/ap2.jpg') }}" alt="">
							<a class="category-post fashion" href="fashion.html">fashion</a>
						</div>
						<div class="post-content">
							<h2><a href="single-post.html">Pellentesque aliquet nibh nec urna. </a></h2>
							<ul class="post-tags">
								<li><i class="fa fa-clock-o"></i>27 may 2013</li>
								<li><i class="fa fa-user"></i>by <a href="#">John Doe</a></li>
								<li><a href="#"><i class="fa fa-comments-o"></i><span>23</span></a></li>
								<li><i class="fa fa-eye"></i>872</li>
							</ul>
						</div>
					</div>
				</div>

				<div class="item">
					<div class="news-post image-post4">
						<div class="post-gallery">
							<img src="{{ asset('hot/nunforest.com/hotmagazine/default/upload/news-posts/ap3.jpg') }}" alt="">
							<a class="category-post travel" href="travel.html">travel</a>
						</div>
						<div class="post-content">
							<h2><a href="single-post.html">Pellentesque aliquet nibh nec urna. </a></h2>
							<ul class="post-tags">
								<li><i class="fa fa-clock-o"></i>27 may 2013</li>
								<li><i class="fa fa-user"></i>by <a href="#">John Doe</a></li>
								<li><a href="#"><i class="fa fa-comments-o"></i><span>23</span></a></li>
								<li><i class="fa fa-eye"></i>872</li>
							</ul>
						</div>
					</div>
				</div>

				<div class="item">
					<div class="news-post image-post4">
						<div class="post-gallery">
							<img src="{{ asset('hot/nunforest.com/hotmagazine/default/upload/news-posts/ap1.jpg') }}" alt="">
							<a class="category-post sport" href="sport.html">sport</a>
						</div>
						<div class="post-content">
							<h2><a href="single-post.html">Pellentesque aliquet nibh nec urna. </a></h2>
							<ul class="post-tags">
								<li><i class="fa fa-clock-o"></i>27 may 2013</li>
								<li><i class="fa fa-user"></i>by <a href="#">John Doe</a></li>
								<li><a href="#"><i class="fa fa-comments-o"></i><span>23</span></a></li>
								<li><i class="fa fa-eye"></i>872</li>
							</ul>
						</div>
					</div>
				</div>

				<div class="item">
					<div class="news-post image-post4">
						<div class="post-gallery">
							<img src="{{ asset('hot/nunforest.com/hotmagazine/default/upload/news-posts/ap2.jpg') }}" alt="">
							<a class="category-post travel" href="travel.html">travel</a>
						</div>
						<div class="post-content">
							<h2><a href="single-post.html">Pellentesque aliquet nibh nec urna. </a></h2>
							<ul class="post-tags">
								<li><i class="fa fa-clock-o"></i>27 may 2013</li>
								<li><i class="fa fa-user"></i>by <a href="#">John Doe</a></li>
								<li><a href="#"><i class="fa fa-comments-o"></i><span>23</span></a></li>
								<li><i class="fa fa-eye"></i>872</li>
							</ul>
						</div>
					</div>
				</div> -->

			</div>
		</div>

	</div>

</section>
<!-- End heading-news4-section -->

<!-- features-today-section
	================================================== -->
<section class="features-today second-style">
	<div class="container">

		<div class="title-section">
			<h1><span>Artikel Kesenian</span></h1>
		</div>

		<div class="features-today-box owl-wrapper">
			<div class="owl-carousel" data-num="4">
				@foreach($hasilArtikelTerpopuler as $data)
				<div class="item news-post standard-post">
					<div class="post-gallery">
						<img src="assets/gambar_artikel/{{ $data->gambar_artikel1 }}" alt="">
						<a class="category-post food" href="#">{{ $data->nama_sub_kategori }}</a>
					</div>
					<div class="post-content">
						<h2><a href="{{ url('/detail-artikel/baca/'.$data->slug1) }}">{{ $data->judul_id }}</a></h2>
						<ul class="post-tags">
							<li><i class="fa fa-clock-o"></i>{{ \Carbon\Carbon::parse($data->created_at)->format('d/m/Y H:i:s') }}</li>
							<li><i class="fa fa-user"></i>by <a href="#">{{ $data->nama_penulis }}</a></li>
							<!-- <li><a href="#"><i class="fa fa-comments-o"></i><span>23</span></a></li> -->
						</ul>
					</div>
				</div>
				@endforeach

			</div>
		</div>

	</div>
</section>
<!-- End features-today-section -->

<!-- block-wrapper-section
	================================================== -->
<section class="block-wrapper">
	<div class="container">

		<!-- block content -->
		<div class="block-content non-sidebar">

			<!-- google addsense -->
			<div class="advertisement">
				<div class="desktop-advert">
					<span>IKLAN</span>
					<img src="{{ asset('hot/nunforest.com/hotmagazine/default/upload/addsense/728x90-white.jpg') }}" alt="">
				</div>
				<div class="tablet-advert">
					<span>IKLAN</span>
					<img src="{{ asset('hot/nunforest.com/hotmagazine/default/upload/addsense/468x60-white.jpg') }}" alt="">
				</div>
				<div class="mobile-advert">
					<span>IKLAN</span>
					<img src="{{ asset('hot/nunforest.com/hotmagazine/default/upload/addsense/300x250.jpg') }}" alt="">
				</div>
			</div>
			<!-- End google addsense -->

			<!-- grid box -->
			<!-- <div class="grid-box">

				<div class="row">

					<div class="col-md-6">
						<div class="title-section">
							<h1><span class="world">Kesenian</span></h1>
						</div>

						<div class="news-post image-post2">
							<div class="post-gallery">
								<img src="{{ asset('hot/nunforest.com/hotmagazine/default/upload/news-posts/hm1.jpg') }}" alt="">
								<div class="hover-box">
									<div class="inner-hover">
										<h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </a></h2>
										<ul class="post-tags">
											<li><i class="fa fa-clock-o"></i>27 may 2013</li>
											<li><i class="fa fa-user"></i>by <a href="#">John Doe</a></li>
											<li><a href="#"><i class="fa fa-comments-o"></i><span>23</span></a></li>
											<li><i class="fa fa-eye"></i>872</li>
										</ul>
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-6">

								<div class="item news-post standard-post">
									<div class="post-gallery">
										<img src="{{ asset('hot/nunforest.com/hotmagazine/default/upload/news-posts/st7.jpg') }}" alt="">
									</div>
									<div class="post-content">
										<h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </a></h2>
										<ul class="post-tags">
											<li><i class="fa fa-clock-o"></i>27 may 2013</li>
											<li><i class="fa fa-user"></i>by <a href="#">John Doe</a></li>
											<li><a href="#"><i class="fa fa-comments-o"></i><span>23</span></a></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-md-6">

								<div class="item news-post standard-post">
									<div class="post-gallery">
										<img src="{{ asset('hot/nunforest.com/hotmagazine/default/upload/news-posts/st8.jpg') }}" alt="">
									</div>
									<div class="post-content">
										<h2><a href="single-post.html">Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.</a></h2>
										<ul class="post-tags">
											<li><i class="fa fa-clock-o"></i>27 may 2013</li>
											<li><i class="fa fa-user"></i>by <a href="#">John Doe</a></li>
											<li><a href="#"><i class="fa fa-comments-o"></i><span>23</span></a></li>
										</ul>
									</div>
								</div>
							</div>

						</div>
					</div>

					<div class="col-md-6">
						<div class="title-section">
							<h1><span class="travel">Kegiatan</span></h1>
						</div>

						<div class="news-post image-post2">
							<div class="post-gallery">
								<img src="{{ asset('hot/nunforest.com/hotmagazine/default/upload/news-posts/hm2.jpg') }}" alt="">
								<div class="hover-box">
									<div class="inner-hover">
										<h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </a></h2>
										<ul class="post-tags">
											<li><i class="fa fa-clock-o"></i>27 may 2013</li>
											<li><i class="fa fa-user"></i>by <a href="#">John Doe</a></li>
											<li><a href="#"><i class="fa fa-comments-o"></i><span>23</span></a></li>
											<li><i class="fa fa-eye"></i>872</li>
										</ul>
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-6">

								<div class="item news-post standard-post">
									<div class="post-gallery">
										<img src="{{ asset('hot/nunforest.com/hotmagazine/default/upload/news-posts/st9.jpg') }}" alt="">
									</div>
									<div class="post-content">
										<h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </a></h2>
										<ul class="post-tags">
											<li><i class="fa fa-clock-o"></i>27 may 2013</li>
											<li><i class="fa fa-user"></i>by <a href="#">John Doe</a></li>
											<li><a href="#"><i class="fa fa-comments-o"></i><span>23</span></a></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-md-6">

								<div class="item news-post standard-post">
									<div class="post-gallery">
										<img src="{{ asset('hot/nunforest.com/hotmagazine/default/upload/news-posts/st3.jpg') }}" alt="">
									</div>
									<div class="post-content">
										<h2><a href="single-post.html">Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.</a></h2>
										<ul class="post-tags">
											<li><i class="fa fa-clock-o"></i>27 may 2013</li>
											<li><i class="fa fa-user"></i>by <a href="#">John Doe</a></li>
											<li><a href="#"><i class="fa fa-comments-o"></i><span>23</span></a></li>
										</ul>
									</div>
								</div>
							</div>

						</div>

					</div>

				</div>

			</div> -->
			<!-- End grid box -->

		</div>
		<!-- End block content -->
	</div>
</section>
<!-- End block-wrapper-section -->



<!-- block-wrapper-section
	================================================== -->
<section class="block-wrapper">
	<div class="container">
		<div class="row">
			<div class="col-sm-8 content-blocker">

				<!-- block content -->
				<div class="block-content">

					<!-- masonry box -->
					<div class="masonry-box">

						<div class="title-section">
							<h1><span>Artikel Terbaru</span></h1>
						</div>

						<div class="latest-articles iso-call">

							<div class="news-post standard-post3 default-size">
								<div class="post-gallery">
									<img src="{{ asset('hot/nunforest.com/hotmagazine/default/upload/news-posts/7.jpg') }}" alt="">
								</div>
								<div class="post-title">
									<a class="category-post tech" href="tech.html">tech</a>
									<h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </a></h2>
									<ul class="post-tags">
										<li><i class="fa fa-clock-o"></i>27 may 2013</li>
										<li><i class="fa fa-user"></i>by <a href="#">John Doe</a></li>
										<li><a href="#"><i class="fa fa-comments-o"></i><span>23</span></a></li>
									</ul>
								</div>
							</div>

							<div class="news-post standard-post3">
								<div class="post-gallery">
									<img src="{{ asset('hot/nunforest.com/hotmagazine/default/upload/news-posts/8.jpg') }}" alt="">
								</div>
								<div class="post-title">
									<a class="category-post travel" href="travel.html">travel</a>
									<h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </a></h2>
									<ul class="post-tags">
										<li><i class="fa fa-clock-o"></i>27 may 2013</li>
										<li><i class="fa fa-user"></i>by <a href="#">John Doe</a></li>
										<li><a href="#"><i class="fa fa-comments-o"></i><span>23</span></a></li>
									</ul>
								</div>
							</div>

							<div class="news-post standard-post3">
								<div class="post-gallery">
									<img src="{{ asset('hot/nunforest.com/hotmagazine/default/upload/news-posts/9.jpg') }}" alt="">
								</div>
								<div class="post-title">
									<a class="category-post fashion" href="fashion.html">fashion</a>
									<h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </a></h2>
									<ul class="post-tags">
										<li><i class="fa fa-clock-o"></i>27 may 2013</li>
										<li><i class="fa fa-user"></i>by <a href="#">John Doe</a></li>
										<li><a href="#"><i class="fa fa-comments-o"></i><span>23</span></a></li>
									</ul>
								</div>
							</div>

							<div class="news-post standard-post3">
								<div class="post-gallery">
									<img src="{{ asset('hot/nunforest.com/hotmagazine/default/upload/news-posts/10.jpg') }}" alt="">
								</div>
								<div class="post-title">
									<a class="category-post sport" href="sport.html">sport</a>
									<h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </a></h2>
									<ul class="post-tags">
										<li><i class="fa fa-clock-o"></i>27 may 2013</li>
										<li><i class="fa fa-user"></i>by <a href="#">John Doe</a></li>
										<li><a href="#"><i class="fa fa-comments-o"></i><span>23</span></a></li>
									</ul>
								</div>
							</div>

							<div class="news-post standard-post3">
								<div class="post-gallery">
									<img src="{{ asset('hot/nunforest.com/hotmagazine/default/upload/news-posts/11.jpg') }}" alt="">
								</div>
								<div class="post-title">
									<a class="category-post sport" href="sport.html">sport</a>
									<h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </a></h2>
									<ul class="post-tags">
										<li><i class="fa fa-clock-o"></i>27 may 2013</li>
										<li><i class="fa fa-user"></i>by <a href="#">John Doe</a></li>
										<li><a href="#"><i class="fa fa-comments-o"></i><span>23</span></a></li>
									</ul>
								</div>
							</div>

							<div class="news-post standard-post3">
								<div class="post-gallery">
									<img src="{{ asset('hot/nunforest.com/hotmagazine/default/upload/news-posts/12.jpg') }}" alt="">
								</div>
								<div class="post-title">
									<a class="category-post travel" href="travel.html">travel</a>
									<h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </a></h2>
									<ul class="post-tags">
										<li><i class="fa fa-clock-o"></i>27 may 2013</li>
										<li><i class="fa fa-user"></i>by <a href="#">John Doe</a></li>
										<li><a href="#"><i class="fa fa-comments-o"></i><span>23</span></a></li>
									</ul>
								</div>
							</div>

						</div>

					</div>
					<!-- End masonry box -->

					<!-- pagination box -->
					<div class="pagination-box">
						<ul class="pagination-list">
							<li><a class="active" href="#">1</a></li>
							<li><a href="#">2</a></li>
							<li><a href="#">3</a></li>
							<li><span>...</span></li>
							<li><a href="#">9</a></li>
							<li><a href="#">Next</a></li>
						</ul>
						<p>Page 1 of 9</p>
					</div>
					<!-- End pagination box -->

				</div>
				<!-- End block content -->

			</div>

			<div class="col-sm-4 sidebar-sticky">

				<!-- sidebar -->
			@include('layout.partials.sidebar_kanan')
				<!-- End sidebar -->

			</div>

		</div>

	</div>
</section>
<!-- End block-wrapper-section -->
<!-- latest-videos-section
	================================================== -->
<section class="latest-videos-section" style="background: #02943f;">
	<div class="container">
		<div class="title-section">
			<h1><span>Video Terpopuler</span></h1>
		</div>

		<!-- slider-caption-box -->
		<div class="grid-box">
			@foreach($hasilVideoTerpopuler as $item)

				<div class="col-md-4">
					<div class="news-post video-post">
						 <img src="https://img.youtube.com/vi/{{ $item->link_video }}/0.jpg"/>
						<a href="https://www.youtube.com/watch?v={{ $item->link_video }}" class="video-link"><i class="fa fa-play-circle-o"></i></a>
						<div class="hover-box">
							<h2><a href="#">{{ $item->judul_id }} </a></h2>
						</div>
					</div>
				</div>


			@endforeach
		</div>
		<!-- End slider-caption-box -->
	</div>
</section>
<a href="https://wa.me/+62895339608375?text=Halo,%20satria,%20Saya%20ingin%20konsultasi.%20Nama:%20Alamat:%20." class="float" target="_blank">
<i class="fab fa-whatsapp my-float"></i>
</a>

<!-- End latest-videos-section -->
@endsection