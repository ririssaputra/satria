@extends('layouts.admin')
@section('content')
<div id="artikel" class="col-lg-12 layout-spacing">
   <div class="statbox widget box box-shadow">
      <div class="widget-content widget-content-area">
         @if (session('pesanError'))
         <div class="alert alert-block alert-danger">
            <a class="close" data-dismiss="alert" href="#">×</a>
            <h4 class="alert-heading">Gagal !</h4>
            {{ session('pesanError') }}<br>
         </div>
         @endif
         <form class="simple-example" method="POST" action="{{ url('/galeri-edit/update/'.$hasilId) }}" enctype="multipart/form-data" novalidate>
            {{ csrf_field() }}
            <h5>Perbaharui Data Galeri</h5>
            <hr />
            <div class="form-row">
               <div class="col-md-6 mb-4">
                  <label for="fullName">Nama Tokoh</label>
                  <input type="text" class="form-control" name="txtNama" id="fullName" placeholder="" value="{{ $tampilGaleri[0]->nama_tokoh }}">
               </div>
            </div>
            <hr />
            <div class="form-row">
               <div class="col-md-6 mb-4">
                  <label for="fullName">Foto Tokoh</label>
                  <div class="custom-file-container" data-upload-id="myFirstImage">
                     <input type="file" name="gambar">
                  </div>
               </div>
            </div>
            <br>
            <div class="form-row">
               <div class="col-md-12 mb-4">
                  <label for="fullName">Foto Sebelum nya</label>
                  <div class="col-md-12 mb-4">
                     <img src="../assets/foto_galeri/{{ $tampilGaleri[0]->foto_tokoh }}" width="400px">
                  </div>
               </div>
            </div>
            <hr />
            <div class="form-row">
               <div class="col-md-6 mb-4">
                  <label for="fullName">Jabatan</label>
                  <input type="text" class="form-control" name="txtJabatan" id="" value="{{ $tampilGaleri[0]->jabatan_tokoh }}">
               </div>
            </div>

            <button class="btn btn-primary submit-fn mt-2" type="submit">Perbaharui</button>
         </form>
      </div>
   </div>
</div>
<!--  END CONTENT AREA  -->
@endsection
