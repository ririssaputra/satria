@extends('layouts.admin')
@section('content')

<div id="artikel" class="col-lg-12 layout-spacing">
   <div class="statbox widget box box-shadow">
      <div class="widget-content widget-content-area">
         @if (session('pesanError'))
         <div class="alert alert-block alert-danger">
            <a class="close" data-dismiss="alert" href="#">×</a>
            <h4 class="alert-heading">Gagal !</h4>
            {{ session('pesanError') }}<br>
         </div>
         @endif
         <form class="simple-example" method="POST" action="{{ url('/galeri-tambah/input') }}" enctype="multipart/form-data" novalidate>
            {{ csrf_field() }}
            <h5>Input Galeri</h5>
            <hr />
            <div class="form-row">
               <div class="col-md-6 mb-4">
                  <label for="fullName">Nama Tokoh</label>
                  <input type="text" class="form-control" name="txtNama" id="fullName" placeholder="" value="">
               </div>
            </div>
            <hr />
            <div class="form-row">
               <div class="col-md-6 mb-4">
                  <label for="fullName">Foto Tokoh</label>
                  <div class="custom-file-container" data-upload-id="myFirstImage">
                     <input type="file" name="gambar">
                  </div>
                  <br />
               </div>
            </div>
              <hr />
            <div class="form-row">
              <div class="col-md-6 mb-4">
                <label for="fullName">Jabatan</label>
                <input type="text" class="form-control" name="txtJabatan" id="" >
              </div>
            </div>

            <button class="btn btn-primary submit-fn mt-2" type="submit">Tambah</button>
         </form>
      </div>
   </div>
</div>
<!--  END CONTENT AREA  -->
@endsection
