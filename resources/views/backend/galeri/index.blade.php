@extends('layouts.admin')

@section('content')
<div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
    <div class="widget-content widget-content-area br-6">
        <a href="{{ url('galeri-tambah') }}" class="btn btn-primary mb-3 rounded">
            Input Galeri
        </a>
        @if (session('msg'))
        <div class="alert alert-block alert-success">
        	<a class="close" data-dismiss="alert" href="#">×</a>
        	<h4 class="alert-heading">Sukses !</h4>
        			{{ session('msg') }}<br>
        </div>
        @endif
        <div class="table-responsive mb-4 mt-4">
            <table id="html5-extension" class="table table-hover non-hover" style="width:100%">
                <thead>
                    <tr>
                        <th>Nama Tokoh</th>
                        <th>Foto Tokoh</th>
                        <th>Jabatan</th>
                        <th>Opsi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($tampilGaleri as $item)
                    <tr>
                        <td>{{ $item->nama_tokoh }}</td>
                        <td><img src="assets/foto_galeri/{{ $item->foto_tokoh }}" width="175px"></td>
                        <td>{{ $item->jabatan_tokoh }}</td>
                        <td>
                            <div class="btn-group">
                                <button type="button" class="btn btn-dark btn-sm">Pratinjau</button>
                                <button type="button" class="btn btn-dark btn-sm dropdown-toggle dropdown-toggle-split" id="dropdownMenuReference1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-reference="parent">
                                  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down"><polyline points="6 9 12 15 18 9"></polyline></svg>
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuReference1">
                                  <a class="dropdown-item" href="{{ url('galeri-edit/'.$item->id_galeri) }}">Perbaharui</a>
                                  <a class="dropdown-item" href="{{ url('galeri-delete/'.$item->id_galeri) }}" onclick="return confirm('Yakin di hapus ?')">Hapus</a>
                                </div>
                              </div>
                        </td>
                    </tr>
                    @endforeach

                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
