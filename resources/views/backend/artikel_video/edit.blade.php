@extends('layouts.admin')
@section('content')
<!-- <div id="navSection" data-spy="affix" class="nav  sidenav">
   <div class="sidenav-content">
       <a href="#artikel" class="active nav-link">Input Artikel</a>
   </div>
   </div> -->
<!-- <div class="row layout-top-spacing"> -->
<div id="artikel" class="col-lg-12 layout-spacing">
   <div class="statbox widget box box-shadow">
      <div class="widget-content widget-content-area">
         @if (session('pesanError'))
         <div class="alert alert-block alert-danger">
            <a class="close" data-dismiss="alert" href="#">×</a>
            <h4 class="alert-heading">Gagal !</h4>
            {{ session('pesanError') }}<br>
         </div>
         @endif
         <form class="simple-example" method="POST" action="{{ url('/artikel-video-edit/update/'.$hasilId) }}" enctype="multipart/form-data" novalidate>
            {{ csrf_field() }}
            <h5>Perbaharui Artikel Video</h5>
            <hr />
            <div class="form-row">
               <div class="col-md-6 mb-4">
                  <label for="fullName">Judul video</label>
                  <input type="text" class="form-control" name="txtJudul" id="fullName" placeholder="" value="{{ $tampilArtikelVideo[0]->judul_id }}">
               </div>
               <!-- <div class="col-md-6 mb-4">
                  <label for="fullName">Judul Artikel English</label>
                  <input type="text" class="form-control" name="txtJudul2" id="fullName" placeholder="" value="">
               </div> -->
            </div>
            <!-- <div class="form-row">
                <div class="col-md-6 mb-4">
                 <label for="exampleFormControlSelect1">Halaman Artikel</label>
                 <select name="txtKategori" class="form-control" onchange="sub_menu()" id="jkategori">
                    <option value="">-- Pilih Halaman Artikel --</option>
                    @foreach($tampilKategori as $item)
                    <option value="{{ $item->id_kategori }}">{{ $item->nama_kategori }}</option>
                    @endforeach
                 </select>
               </div>


            </div> -->
            <hr />
            <div class="form-row">
               <div class="col-md-4 mb-4">
                  <label for="fullName">Code embed (Contoh : <b><u>oE8-xUyDSk8</u></b>)</label>

               </div>
               <div class="col-md-4 mb-4">
                  <label for="fullName">Sumber video (Contoh : youtube)</label>
               </div>
               <div class="col-md-4 mb-4">
                 <label for="exampleFormControlSelect1">Editor video</label>
              </div>
              <div class="col-md-4 mb-4">
                  <input type="text" class="form-control" name="link_video" id="fullName" placeholder="" value="{{ $tampilArtikelVideo[0]->link_video }}">
              </div>
              <div class="col-md-4 mb-4">
                  <input type="text" class="form-control" name="txtSumber" id="fullName" placeholder="" value="{{ $tampilArtikelVideo[0]->sumber_video }}">
              </div>
              <div class="col-md-4 mb-4">
                  <select name="txtEditor" class="form-control" id="exampleFormControlSelect1">
                     <option value="">-- Pilih Editor Video --</option>
                     @foreach($tampilEditor as $item3)
                       <option value="{{ $item3->id_editor_video }}" @if($item3->id_editor_video == $tampilArtikelVideo[0]->id_editor_video) selected @endif>{{ $item3->nama_editor }}</option>
                     @endforeach
                  </select>
              </div>
            </div>
            <div class="form-row">
               <div class="col-md-12 mb-4">
                  <label for="fullName">Deksripsi video</label>
                  <textarea class="form-control" name="txtIsi" id="" rows="4" cols="50" required>{{ $tampilArtikelVideo[0]->isi_artikel_id1 }}</textarea>
               </div>
            </div>
            <!-- <div class="form-row">
               <div class="col-md-12 mb-4">
                  <label for="fullName">Paragraf 2 Indonesia</label>
                  <textarea class="form-control" name="txtIsi2" id="" rows="4" cols="50"></textarea>
               </div>
            </div> -->
            <!-- <div class="form-row">
               <div class="col-md-12 mb-4">
                  <label for="fullName">Paragraf 1 English</label>
                  <textarea class="form-control" name="txtIsi3" id="" rows="4" cols="50"></textarea>
               </div>
            </div> -->
            <!-- <div class="form-row">
               <div class="col-md-12 mb-4">
                  <label for="fullName">Paragraf 2 English</label>
                  <textarea class="form-control" name="txtIsi4" id="" rows="4" cols="50"></textarea>
               </div>
            </div> -->
            <div class="form-row">
               <div class="col-md-12 mb-4">
                  <label for="fullName">Keyword / Meta tag</label>
                  <input type="text" class="form-control" name="txtMeta" id="" value="{{ $tampilArtikelVideo[0]->keyword }}">
               </div>
            </div>
            <!-- <div class="form-row">
               <div class="col-md-6 mb-4">
                  <label for="fullName">Gambar Utama</label>
                  <div class="custom-file-container" data-upload-id="myFirstImage">
                     <input type="file" name="gambar">
                  </div>
                  <br />
                  <label for="fullName">Info / Tautan Hak Cipta Gambar Utama</label>
                  <input type="text" class="form-control" name="txtInfoGambar" id="fullName" placeholder="" value="">
               </div>
               <div class="col-md-6 mb-4">
                  <label for="fullName">Gambar Tambahan</label>
                  <div class="custom-file-container" data-upload-id="myFirstImage">
                     <input type="file" name="gambar2">
                  </div>
                  <br />
                  <label for="fullName">Info / Tautan Hak Cipta Gambar Tambahan</label>
                  <input type="text" class="form-control" name="txtInfoGambar2" id="fullName" placeholder="" value="">
                </div>
            </div> -->

            <!-- <div class="form-group mb-4">
               <label for="exampleFormControlSelect1">Sub Kategori Artikel</label>
               <div id="div1">
               <select name="txtSubKategori2" class="form-control">
                 <option value="">-- Pilih Sub Kategori Artikel --</option>
               </select>
               </div>
               <div id="wait" class="loader-halaman"></div>
               </div> -->
            <button class="btn btn-primary submit-fn mt-2" type="submit">Perbaharui</button>
         </form>
      </div>
   </div>
</div>
<!--  END CONTENT AREA  -->
@endsection
