@extends('layouts.admin')

@section('content')
<div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
    <div class="widget-content widget-content-area br-6">
        <a href="{{ url('artikel-video-tambah') }}" class="btn btn-primary mb-3 rounded">
            Input Artikel Video
        </a>
        @if (session('msg'))
        <div class="alert alert-block alert-success">
        	<a class="close" data-dismiss="alert" href="#">×</a>
        	<h4 class="alert-heading">Sukses !</h4>
        			{{ session('msg') }}<br>
        </div>
        @endif
        <div class="table-responsive mb-4 mt-4">
            <table id="html5-extension" class="table table-hover non-hover" style="width:100%">
                <thead>
                    <tr>
                        <th>Judul Video</th>
                        <th>Code Embed</th>
                        <th>Thumbnail</th>
                        <th>Jumlah Tayang</th>
                        <th>Waktu Publish</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($tampilVideo as $item)
                    <tr>
                        <td>{{ $item->judul_id }}</td>
                        <td><a href="https://youtu.be/{{ $item->link_video}}" target="_blank">{{ $item->link_video}}</a></td>
                        <td><img src="https://img.youtube.com/vi/{{ $item->link_video }}/default.jpg"/></td>
                        <td>{{ $item->hitung_tayang }}x</td>
                        <td>{{ \Carbon\Carbon::parse($item->created_at)->format('d/m/Y H:i:s') }}</td>
                        <td>
                            <div class="btn-group">
                                <button type="button" class="btn btn-dark btn-sm">Pratinjau</button>
                                <button type="button" class="btn btn-dark btn-sm dropdown-toggle dropdown-toggle-split" id="dropdownMenuReference1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-reference="parent">
                                  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down"><polyline points="6 9 12 15 18 9"></polyline></svg>
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuReference1">
                                  <a class="dropdown-item" href="{{ url('artikel-video-edit/'.$item->id_artikel) }}">Perbaharui</a>
                                  <a class="dropdown-item" href="{{ url('artikel-video-delete/'.$item->id_artikel) }}" onclick="return confirm('Yakin di hapus ?')">Hapus</a>
                                  <!-- <div class="dropdown-divider"></div>
                                  <a class="dropdown-item" href="#">Separated link</a> -->
                                </div>
                              </div>
                        </td>
                    </tr>
                    @endforeach

                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
