@extends('layouts.admin')

@section('content')
<div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
  <div class="widget-content widget-content-area br-6">
    @if (session('pesanError'))
    <div class="alert alert-block alert-danger">
       <a class="close" data-dismiss="alert" href="#"></a>
       <h4 class="alert-heading">Gagal !</h4>
       {{ session('pesanError') }}<br>
    </div>
    @endif
      <!-- <div class="statbox widget box box-shadow"> -->
          <div class="widget-header">
              <div class="row">
                  <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                      <h4>&nbsp;&nbsp;Kategori Menu</h4>
                  </div>
              </div>
          </div>
          <div class="widget-content widget-content-area">
            <form class="simple-example" method="POST" action="{{ url('/kategori-tambah/input') }}" enctype="multipart/form-data" novalidate>
              {{ csrf_field() }}
              <div class="row">
                  <div class="col-lg-6 col-12 mx-auto">
                      <div class="form-group">
                          <label for="exampleFormControlInput1">Nama Kategori</label>
                          <input type="text" class="form-control" name="txtNamaKategori" id="exampleFormControlInput1" value="">
                          <div class="valid-feedback">
                              Kerja Bagus!
                          </div>
                          <div class="invalid-feedback">
                              Masukkan field nama kategori
                          </div>
                      </div>
                  </div>
              </div><br>
              <!-- <div class="row">
                  <div class="col-lg-6 col-12 mx-auto">
                      <div class="form-group">
                        <label for="exampleFormControlFile1">Gambar Kategori</label>
                        <input type="file" class="form-control-file" name="gambar" id="exampleFormControlFile1">
                      </div>
                  </div>
              </div><br><br> -->
              <div class="row">
                  <div class="col-lg-6 col-12 mx-auto">
                      <div class="form-group">
                        <button class="btn btn-primary submit-fn mt-2" type="submit">Simpan</button>
                      </div>
                  </div>
              </div>
            </form>
          </div>
      <!-- </div> -->

  </div>
</div>
<script>
    $(document).ready(function() {
        App.init();
    });
</script>

<script>
    $('#zero-config').DataTable({
        "oLanguage": {
            "oPaginate": { "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>', "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>' },
            "sInfo": "Showing page _PAGE_ of _PAGES_",
            "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
            "sSearchPlaceholder": "Search...",
           "sLengthMenu": "Results :  _MENU_",
        },
        "stripeClasses": [],
        "lengthMenu": [7, 10, 20, 50],
        "pageLength": 7
    });
</script>
@endsection
