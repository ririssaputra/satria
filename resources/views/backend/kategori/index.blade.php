@extends('layouts.admin')

@section('content')
<div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
  <div class="widget-content widget-content-area br-6">
      <div class="table-responsive mb-4 mt-4">
        @if (session('msg'))
			<div class="alert alert-block alert-success">
				<a class="close" data-dismiss="alert" href="#">×</a>
				<h4 class="alert-heading">Sukses !</h4>
						{{ session('msg') }}<br>
			</div>
			@endif
          <table id="zero-config" class="table table-hover" style="width:100%">
            <a href="{{ url('/kategori-tambah') }}" class="btn btn-primary mb-3 rounded bs-tooltip" title="untuk halaman menu">
                Tambah Kategori Menu
            </a>
              <thead>
                  <tr>
                      <th>Nama Kategori</th>
                      <!-- <th>Gambar Kategori</th> -->
                      <!-- <th>Status</th> -->
                      <th>Opsi</th>
                  </tr>
              </thead>
              <tbody>
                @foreach($tampilKategori as $item)
                  <tr>
                      <td>{{$item->nama_kategori}}</td>

                      <td>
                        <a class="btn btn-outline-warning mb-2" href="{{ url('kategori-edit/'.$item->id_kategori) }}">Edit</a>
                        <a class="btn btn-outline-danger mb-2" href="{{ url('kategori-delete/'.$item->id_kategori) }}" onclick="return confirm('Yakin di hapus ?')">Delete</a>
                          <!-- <div class="btn-group">
                              <button type="button" class="btn btn-dark btn-sm">Lihat</button>
                              <button type="button" class="btn btn-dark btn-sm dropdown-toggle dropdown-toggle-split" id="dropdownMenuReference1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-reference="parent">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down"><polyline points="6 9 12 15 18 9"></polyline></svg>
                              </button>
                              <div class="dropdown-menu" aria-labelledby="dropdownMenuReference1">
                                <a class="dropdown-item" href="">Sub</a>
                                <a class="dropdown-item" href="{{ url('kategori-edit/'.$item->id_kategori) }}">Edit</a>
                                <a class="dropdown-item" href="">Hapus</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#">Separated link</a>
                              </div>
                            </div> -->
                      </td>
                  </tr>
                  @endforeach
              </tbody>
              <tfoot>
                  <tr>
                    <th>Nama Kategori</th>
                    <!-- <th></th> -->
                    <!-- <th>Status</th> -->
                    <!-- <th></th> -->
                  </tr>
              </tfoot>
          </table>
      </div>
  </div>
</div>
<script>
    $(document).ready(function() {
        App.init();
    });
</script>

<script src="{{ asset('temp/plugins/table/datatable/datatables.js')}}"></script>
<script>
    $('#zero-config').DataTable({
        "oLanguage": {
            "oPaginate": { "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>', "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>' },
            "sInfo": "Showing page _PAGE_ of _PAGES_",
            "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
            "sSearchPlaceholder": "Search...",
           "sLengthMenu": "Results :  _MENU_",
        },
        "stripeClasses": [],
        "lengthMenu": [7, 10, 20, 50],
        "pageLength": 7
    });
</script>
@endsection
