@extends('layouts.admin')
@section('content')
<!-- <div id="navSection" data-spy="affix" class="nav  sidenav">
   <div class="sidenav-content">
       <a href="#artikel" class="active nav-link">Input Artikel</a>
   </div>
   </div> -->
<!-- <div class="row layout-top-spacing"> -->
<div id="artikel" class="col-lg-12 layout-spacing">
   <div class="statbox widget box box-shadow">
      <div class="widget-header">
         <div class="row">
            <div class="col-xl-12 col-md-12 col-sm-12 col-12">
               <h4>Input Artikel</h4>
            </div>
         </div>
      </div>
      <div class="widget-content widget-content-area">
         @if (session('pesanError'))
         <div class="alert alert-block alert-danger">
            <a class="close" data-dismiss="alert" href="#">×</a>
            <h4 class="alert-heading">Sukses !</h4>
            {{ session('pesanError') }}<br>
         </div>
         @endif
         <form class="simple-example" method="POST" action="{{ url('/artikel-tambah/input') }}" enctype="multipart/form-data" novalidate>
            {{ csrf_field() }}
            <div class="form-row">
               <div class="col-md-12 mb-4">
                  <label for="fullName">Judul Artikel Indonesia</label>
                  <input type="text" class="form-control" name="txtJudul" id="fullName" placeholder="" value="">
               </div>
            </div>
            <br>
            <div class="form-row">
               <div class="col-md-12 mb-4">
                  <label for="fullName">Judul Artikel English</label>
                  <input type="text" class="form-control" name="txtJudul2" id="fullName" placeholder="" value="">
               </div>
            </div>
            <br>
            <div class="form-row">
               <div class="col-md-12 mb-4">
                  <label for="fullName">Paragraf 1 Indonesia</label>
                  <textarea class="form-control" name="txtIsi" id="" rows="4" cols="50" required></textarea>
               </div>
            </div>
            <br>
            <div class="form-row">
               <div class="col-md-12 mb-4">
                  <label for="fullName">Paragraf 2 Indonesia</label>
                  <textarea class="form-control" name="txtIsi2" id="" rows="4" cols="50"></textarea>
               </div>
            </div>
            <br>
            <div class="form-row">
               <div class="col-md-12 mb-4">
                  <label for="fullName">Paragraf 1 English</label>
                  <textarea class="form-control" name="txtIsi3" id="" rows="4" cols="50"></textarea>
               </div>
            </div>
            <br>
            <div class="form-row">
               <div class="col-md-12 mb-4">
                  <label for="fullName">Paragraf 2 English</label>
                  <textarea class="form-control" name="txtIsi4" id="" rows="4" cols="50"></textarea>
               </div>
            </div>
            <br>
            <div class="form-row">
               <div class="col-md-12 mb-4">
                  <label for="fullName">Keyword / Meta tag</label>
                  <textarea class="form-control" name="txtMeta" id="" rows="4" cols="50"></textarea>
               </div>
            </div>
            <br>
            <div class="form-group mb-4">
               <label for="exampleFormControlSelect1">Penulis Artikel</label>
               <select name="txtPenulis" class="form-control" id="exampleFormControlSelect1">
                  <option value="">-- Pilih Penulis Artikel --</option>
                  @foreach($tampilPenulis as $item3)
                  <option value="{{ $item3->id_penulis }}">{{ $item3->nama_penulis }}</option>
                  @endforeach
               </select>
            </div>
            <br>
            <div class="form-row">
               <div class="col-md-12 mb-4">
                  <label for="fullName">Gambar Utama</label>
                  <div class="custom-file-container" data-upload-id="myFirstImage">
                     <input type="file" name="gambar">
                  </div>
               </div>
            </div>
            <br>
            <div class="form-row">
               <div class="col-md-12 mb-4">
                  <label for="fullName">Info / Tautan Hak Cipta Gambar Utama</label>
                  <input type="text" class="form-control" name="txtInfo" id="fullName" placeholder="" value="">
               </div>
            </div>
            <br>
            <div class="form-row">
               <div class="col-md-12 mb-4">
                  <label for="fullName">Gambar Tambahan</label>
                  <div class="custom-file-container" data-upload-id="myFirstImage">
                     <input type="file" name="gambar2">
                  </div>
               </div>
            </div>
            <br>
            <div class="form-row">
               <div class="col-md-12 mb-4">
                  <label for="fullName">Info / Tautan Hak Cipta Gambar Tambahan</label>
                  <input type="text" class="form-control" name="txtInfo2" id="fullName" placeholder="" value="">
               </div>
            </div>
            <br>
            <div class="form-group mb-4">
               <label for="exampleFormControlSelect1">Halaman Artikel</label>
               <select name="txtKategori" class="form-control" onchange="sub_menu()" id="jkategori">
                  <option value="">-- Pilih Halaman Artikel --</option>
                  @foreach($tampilKategori as $item)
                  <option value="{{ $item->id_kategori }}">{{ $item->nama_kategori }}</option>
                  @endforeach
               </select>
            </div>
            <!-- <div class="form-group mb-4">
               <label for="exampleFormControlSelect1">Sub Kategori Artikel</label>
               <div id="div1">
               <select name="txtSubKategori2" class="form-control">
                 <option value="">-- Pilih Sub Kategori Artikel --</option>
               </select>
               </div>
               <div id="wait" class="loader-halaman"></div>
               </div> -->
            <button class="btn btn-primary submit-fn mt-2" type="submit">Input Artikel</button>
         </form>
      </div>
   </div>
</div>
<!--  END CONTENT AREA  -->
@endsection
