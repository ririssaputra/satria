@extends('layouts.admin')
@section('content')
<!-- <div id="navSection" data-spy="affix" class="nav  sidenav">
   <div class="sidenav-content">
       <a href="#artikel" class="active nav-link">Perbaharui Artikel</a>
   </div>
   </div> -->
<div id="artikel" class="col-lg-12 layout-spacing">
   <div class="statbox widget box box-shadow">
      <div class="widget-header">
         <div class="row">
            <div class="col-xl-12 col-md-12 col-sm-12 col-12">
               <h4>Perbaharui Artikel</h4>
            </div>
         </div>
      </div>
      <div class="widget-content widget-content-area">
         <form class="simple-example" method="POST" action="{{ url('/artikel-edit/update/'.$hasilId) }}" enctype="multipart/form-data" novalidate>
            {{ csrf_field() }}
            <div class="form-row">
               <div class="col-md-12 mb-4">
                  <label for="fullName">Judul Artikel Indonesia</label>
                  <input type="text" class="form-control" name="txtJudul" id="fullName" placeholder="" value="{{ $tampilArtikel[0]->judul_id }}" required>
               </div>
            </div>
            <br>
            <div class="form-row">
               <div class="col-md-12 mb-4">
                  <label for="fullName">Judul Artikel English</label>
                  <input type="text" class="form-control" name="txtJudul2" id="fullName" placeholder="" value="{{ $tampilArtikel[0]->judul_en }}" required>
               </div>
            </div>
            <br>
            <div class="form-row">
               <div class="col-md-12 mb-4">
                  <label for="fullName">Paragraf 1 Indonesia</label>
                  <textarea class="form-control" name="txtIsi" id="fullName" rows="4" cols="50">{{ $tampilArtikel[0]->isi_artikel_id1 }}</textarea>
               </div>
            </div>
            <br>
            <div class="form-row">
               <div class="col-md-12 mb-4">
                  <label for="fullName">Paragraf 2 Indonesia</label>
                  <textarea class="form-control" name="txtIsi2" id="fullName" rows="4" cols="50">{{ $tampilArtikel[0]->isi_artikel_id2 }}</textarea>
               </div>
            </div>
            <br>
            <div class="form-row">
               <div class="col-md-12 mb-4">
                  <label for="fullName">Paragraf 1 English</label>
                  <textarea class="form-control" name="txtIsi3" id="fullName" rows="4" cols="50">{{ $tampilArtikel[0]->isi_artikel_en1 }}</textarea>
               </div>
            </div>
            <br>
            <div class="form-row">
               <div class="col-md-12 mb-4">
                  <label for="fullName">Paragraf 2 English</label>
                  <textarea class="form-control" name="txtIsi4" id="fullName" rows="4" cols="50">{{ $tampilArtikel[0]->isi_artikel_en2 }}</textarea>
               </div>
            </div>
            <br>
            <div class="form-row">
               <div class="col-md-12 mb-4">
                  <label for="fullName">Keyword / Meta tag</label>
                  <textarea class="form-control" name="txtMeta" id="fullName" rows="4" cols="50">{{ $tampilArtikel[0]->keyword }}</textarea>
               </div>
            </div>
            <br>
            <div class="form-row">
               <div class="col-md-12 mb-4">
                  <label for="fullName">Gambar Utama Sebelum nya</label>
                  <div class="col-md-12 mb-4">
                     <img src="../assets/gambar_artikel/{{ $tampilArtikel[0]->gambar_artikel1 }}" width="400px">
                  </div>
               </div>
            </div>
            <br>
            <div class="form-row">
               <div class="col-md-12 mb-4">
                  <label for="fullName">Ubah Gambar Utama</label>
                  <div class="col-md-12 mb-4">
                     <input type="file" name="gambar">
                  </div>
               </div>
            </div>
            <br>
            <input type="hidden" name="hidgambar" value="{{ $tampilArtikel[0]->gambar_artikel1 }}">
            <div class="form-row">
               <div class="col-md-12 mb-4">
                  <label for="fullName">Info / Tautan Hak Cipta Gambar Utama</label>
                  <input type="text" class="form-control" name="txtInfo" id="fullName" placeholder="" value="{{ $tampilArtikel[0]->info_gambar1 }}">
               </div>
            </div>
            <br>
            <div class="form-row">
               <div class="col-md-12 mb-4">
                  <label for="fullName">Gambar Tambahan Sebelum nya</label>
                  <div class="col-md-12 mb-4">
                     <img src="../assets/gambar_artikel/{{ $tampilArtikel[0]->gambar_artikel2 }}" width="400px">
                  </div>
               </div>
            </div>
            <br>
            <div class="form-row">
               <div class="col-md-12 mb-4">
                  <label for="fullName">Ubah Gambar Tambahan</label>
                  <div class="col-md-12 mb-4">
                     <input type="file" name="gambar2">
                  </div>
               </div>
            </div>
            <br>
            <input type="hidden" name="hidgambar2" value="{{ $tampilArtikel[0]->gambar_artikel2 }}">
            <div class="form-row">
               <div class="col-md-12 mb-4">
                  <label for="fullName">Info / Tautan Hak Cipta Gambar Tambahan</label>
                  <input type="text" class="form-control" name="txtInfo2" id="fullName" placeholder="" value="{{ $tampilArtikel[0]->info_gambar2 }}">
               </div>
            </div>
            <br>
            <div class="form-group mb-4">
               <label for="exampleFormControlSelect1">Halaman Artikel</label>
               <select name="txtKategori" class="form-control" id="exampleFormControlSelect1">
                  <option value="">-- Pilih Halaman Artikel --</option>
                  @foreach($tampilKategori as $item)
                  <option value="{{ $item->id_kategori }}" @if($item->id_kategori == $tampilArtikel[0]->id_kategori) selected @endif>{{ $item->nama_kategori }}</option>
                  @endforeach
               </select>
            </div>
            <br>
            <button class="btn btn-primary submit-fn mt-2" type="submit">Perbaharui Artikel</button>
         </form>
      </div>
   </div>
</div>
</div>
@endsection
