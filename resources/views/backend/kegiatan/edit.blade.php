@extends('layouts.admin')
@section('content')
<div id="artikel" class="col-lg-12 layout-spacing">
   <div class="statbox widget box box-shadow">
      <div class="widget-header">
         <div class="row">
            <div class="col-xl-12 col-md-12 col-sm-12 col-12">
               <h4>Perbaharui Kegiatan</h4>
            </div>
         </div>
      </div>
      <div class="widget-content widget-content-area">
         <form class="simple-example" method="POST" action="{{ url('/kegiatan-edit/update/'.$hasilId) }}" enctype="multipart/form-data" novalidate>
            {{ csrf_field() }}
            <div class="form-row">
               <div class="col-md-6 mb-4">
                  <label for="fullName">Nama Kegiatan</label>
                  <input type="text" class="form-control" name="txtNamaKegiatan" id="fullName" placeholder="" value="{{ $tampilGaleri[0]->nama_keg_id }}" required>
               </div>
            </div>
            <br>
            <!-- <div class="form-row">
               <div class="col-md-6 mb-4">
                  <label for="fullName">Nama Kegiatan</label>
                  <input type="text" class="form-control" name="txtNamaKegiatan2" id="fullName" placeholder="" value="{{ $tampilGaleri[0]->nama_keg_en }}" required>
               </div>
            </div> -->
            <div class="form-row">
               <div class="col-md-12 mb-4">
                  <label for="fullName">Gambar Utama Sebelum nya</label>
                  <div class="col-md-12 mb-4">
                     <img src="../assets/gambar_kegiatan/{{ $tampilGaleri[0]->foto_keg1 }}" width="400px">
                  </div>
               </div>
            </div>
            <br>
            <div class="form-row">
               <div class="col-md-12 mb-4">
                  <label for="fullName">Ubah Gambar Utama</label>
                  <div class="col-md-12 mb-4">
                     <input type="file" name="gambar">
                  </div>
               </div>
            </div>
            <br>
            <input type="hidden" name="hidgambar" value="{{ $tampilGaleri[0]->foto_keg1 }}">

            <br>
            <div class="form-row">
               <div class="col-md-6 mb-4">
                  <label for="fullName">Isi Kegiatan</label>
                  <textarea class="form-control" name="txtIsi" id="fullName" rows="4" cols="50">{{ $tampilGaleri[0]->isi_keg_id }}</textarea>
               </div>
            </div>
            <br>
            <!-- <div class="form-row">
               <div class="col-md-12 mb-4">
                  <label for="fullName">Isi Kegiatan English</label>
                  <textarea class="form-control" name="txtIsi2" id="fullName" rows="4" cols="50">{{ $tampilGaleri[0]->isi_keg_en }}</textarea>
               </div>
            </div>
            <br> -->

            <div class="form-row">
               <div class="col-md-12 mb-4">
                  <label for="fullName">Gambar Tambahan Sebelum nya</label>
                  <div class="col-md-12 mb-4">
                     <img src="../assets/gambar_kegiatan/{{ $tampilGaleri[0]->foto_keg2 }}" width="400px">
                  </div>
               </div>
            </div>
            <br>
            <div class="form-row">
               <div class="col-md-12 mb-4">
                  <label for="fullName">Ubah Gambar Tambahan</label>
                  <div class="col-md-12 mb-4">
                     <input type="file" name="gambar2">
                  </div>
               </div>
            </div>
            <br>
            <input type="hidden" name="hidgambar2" value="{{ $tampilGaleri[0]->foto_keg2 }}">
            <br>
            <div class="form-row">
               <div class="col-md-6 mb-4">
                  <label for="fullName">Tanggal Kegiatan</label>
                  <input type="date" class="form-control" name="txtTgl" id="fullName" placeholder="" value="{{ $tampilGaleri[0]->tgl_kegiatan }}" required>
               </div>
            </div>
            <br>
            <button class="btn btn-primary submit-fn mt-2" type="submit">Perbaharui Kegiatan</button>
         </form>
      </div>
   </div>
</div>
</div>
@endsection
