@extends('layouts.admin')
@section('content')

<div id="artikel" class="col-lg-12 layout-spacing">
   <div class="statbox widget box box-shadow">
      <div class="widget-header">
         <div class="row">
            <div class="col-xl-12 col-md-12 col-sm-12 col-12">
               <h4>Input Kegiatan</h4>
            </div>
         </div>
      </div>
      <div class="widget-content widget-content-area">
        @if (session('pesanError'))
        <div class="alert alert-block alert-danger">
           <a class="close" data-dismiss="alert" href="#">×</a>
           <h4 class="alert-heading">Gagal !</h4>
           {{ session('pesanError') }}<br>
        </div>
        @endif
         <form class="simple-example" method="POST" action="{{ url('/kegiatan-tambah/input') }}" enctype="multipart/form-data" novalidate>
            {{ csrf_field() }}
            <div class="form-row">
               <div class="col-md-6 mb-4">
                  <label for="fullName">Nama Kegiatan</label>
                  <input type="text" class="form-control" name="txtNamaKegiatan" id="fullName" placeholder="" value="">
               </div>
            </div>
            <br>
            <!-- <div class="form-row">
               <div class="col-md-12 mb-4">
                  <label for="fullName">Nama Kegiatan English</label>
                  <input type="text" class="form-control" name="txtNamaKegiatan2" id="fullName" placeholder="" value="">
               </div>
            </div>
            <br> -->
            <div class="form-row">
               <div class="col-md-12 mb-4">
                  <label for="fullName">Gambar Kegiatan Utama</label>
                  <div class="custom-file-container" data-upload-id="myFirstImage">
                     <input type="file" name="gambar">
                  </div>
               </div>
            </div>
            <br>
            <div class="form-row">
               <div class="col-md-12 mb-4">
                  <label for="fullName">Isi Kegiatan</label>
                  <textarea class="form-control" name="txtIsi" id="" rows="4" cols="50" required></textarea>
               </div>
            </div>
            <br>
            <!-- <div class="form-row">
               <div class="col-md-12 mb-4">
                  <label for="fullName">Isi 2 Kegiatan</label>
                  <textarea class="form-control" name="txtIsi2" id="" rows="4" cols="50" required></textarea>
               </div>
            </div>
            <br> -->
            <div class="form-row">
               <div class="col-md-12 mb-4">
                  <label for="fullName">Gambar Kegiatan Tambahan</label>
                  <div class="custom-file-container" data-upload-id="myFirstImage">
                     <input type="file" name="gambar2">
                  </div>
               </div>
            </div>
            <br>
            <div class="form-row">
               <div class="col-md-6 mb-4">
                  <label for="fullName">Tanggal Kegiatan</label>
                  <input type="date" class="form-control" name="txtTgl" id="fullName" placeholder="" value="">
               </div>
            </div>

            <button class="btn btn-primary submit-fn mt-2" type="submit">Tambah  Kegiatan</button>
         </form>
      </div>
   </div>
</div>
<!--  END CONTENT AREA  -->
@endsection
