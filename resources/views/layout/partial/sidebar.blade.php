<div class="sidebar-wrapper sidebar-theme">

    <nav id="sidebar">
        <div class="shadow-bottom"></div>

        <ul class="list-unstyled menu-categories" id="accordionExample">
            <li class="menu">
                <a href="{{ url('/admin')}}" @if($menubar=='beranda') data-active="true" aria-expanded="true" @endif class="dropdown-toggle">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg>
                        <span>Beranda</span>
                    </div>
                </a>
            </li>

            <li class="menu">
                <a href="#users" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-users"><path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="9" cy="7" r="4"></circle><path d="M23 21v-2a4 4 0 0 0-3-3.87"></path><path d="M16 3.13a4 4 0 0 1 0 7.75"></path></svg>
                        <span>Pengguna</span>
                    </div>
                    <div>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>
                    </div>
                </a>
                <ul class="collapse submenu list-unstyled" id="users" data-parent="#accordionExample">
                    <li>
                        <a href="user_profile.html"> Data Anggota </a>
                    </li>
                    <li>
                        <a href="user_account_setting.html"> Data Calon Anggota </a>
                    </li>
                </ul>
            </li>

            <li class="menu">
                <a href="#setting" data-toggle="collapse" @if($menubar=='masterdata') data-active="true" aria-expanded="false" @endif class="dropdown-toggle">
                  <div class="">
                      <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-terminal"><polyline points="4 17 10 11 4 5"></polyline><line x1="12" y1="19" x2="20" y2="19"></line></svg>
                      <span>Master Data </span>
                  </div>
                  <div>
                      <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>
                  </div>
                </a>
                <ul class="collapse submenu list-unstyled @if($active=='master') show @endif" id="setting" data-parent="#accordionExample">
                    <li @if($active=='master') class="active" @endif>
                        <a href="{{ url('kategori') }}"> Kategori Menu</a>
                    </li>
                </ul>
                <ul class="collapse submenu list-unstyled @if($active=='master') show @endif" id="setting" data-parent="#accordionExample">
                    <li @if($active=='master') class="active" @endif>
                        <a href="{{ url('sub-kategori') }}"> Kategori Artikel</a>
                    </li>
                </ul>
                <ul class="collapse submenu list-unstyled @if($active=='master') show @endif" id="setting" data-parent="#accordionExample">
                    <li @if($active=='master') class="active" @endif>
                        <a href="{{ url('penulis') }}"> Penulis Artikel </a>
                    </li>
                </ul>
                <ul class="collapse submenu list-unstyled @if($active=='master') show @endif" id="setting" data-parent="#accordionExample">
                    <li @if($active=='master') class="active" @endif>
                        <a href="{{ url('editor') }}"> Editor Video </a>
                    </li>
                </ul>
            </li>

            <li class="menu">
                <a href="{{ url('/tabloid-buhun')}}" @if($menubar=='buhun') data-active="true" aria-expanded="true" @endif class="dropdown-toggle">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-book-open"><path d="M2 3h6a4 4 0 0 1 4 4v14a3 3 0 0 0-3-3H2z"></path><path d="M22 3h-6a4 4 0 0 0-4 4v14a3 3 0 0 1 3-3h7z"></path></svg>                        <span>Tabloid Buhun</span>
                    </div>
                </a>
            </li>

            <li class="menu">
                <a href="{{ url('/artikel')}}" @if($menubar=='berita') data-active="true" aria-expanded="true" @endif class="dropdown-toggle">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file"><path d="M4 19.5A2.5 2.5 0 0 1 6.5 17H20"></path><path d="M6.5 2H20v20H6.5A2.5 2.5 0 0 1 4 19.5v-15A2.5 2.5 0 0 1 6.5 2z"></path></svg>
                        <span>Artikel</span>
                    </div>
                </a>
            </li>

            <li class="menu">
                <a href="{{ url('/artikel-video')}}" @if($menubar=='artikelvideo') data-active="true" aria-expanded="true" @endif class="dropdown-toggle">
                    <div class="">
                          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-airplay"><path d="M5 17H4a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h16a2 2 0 0 1 2 2v10a2 2 0 0 1-2 2h-1"></path><polygon points="12 15 17 21 7 21 12 15"></polygon></svg>
                        <span>Artikel Video</span>
                    </div>
                </a>
            </li>

            <li class="menu">
                <a href="{{ url('/galeri')}}" @if($menubar=='galeri') data-active="true" aria-expanded="true" @endif class="dropdown-toggle">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-layout"><rect x="3" y="3" width="18" height="18" rx="2" ry="2"></rect><line x1="3" y1="9" x2="21" y2="9"></line><line x1="9" y1="21" x2="9" y2="9"></line></svg>
                      <span>Galeri</span>
                    </div>
                </a>
            </li>

            <li class="menu">
                <a href="{{ url('/kegiatan')}}" @if($menubar=='kegiatan') data-active="true" aria-expanded="true" @endif class="dropdown-toggle">
                    <div class="">
                          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-layers"><polygon points="12 2 2 7 12 12 22 7 12 2"></polygon><polyline points="2 17 12 22 22 17"></polyline><polyline points="2 12 12 17 22 12"></polyline></svg>
                        <span>Kegiatan</span>
                    </div>
                </a>
            </li>

            <li class="menu">
                <a href="{{ url('/perpustakaan')}}" @if($menubar=='perpustakaan') data-active="true" aria-expanded="true" @endif class="dropdown-toggle">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-calendar"><rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect><line x1="16" y1="2" x2="16" y2="6"></line><line x1="8" y1="2" x2="8" y2="6"></line><line x1="3" y1="10" x2="21" y2="10"></line></svg>
                      <span>Perpustakaan</span>
                    </div>
                </a>
            </li>

            <li class="menu">
                <a href="{{ url('/info-kontak')}}" @if($menubar=='info') data-active="true" aria-expanded="true" @endif class="dropdown-toggle">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-map"><polygon points="1 6 1 22 8 18 16 22 23 18 23 2 16 6 8 2 1 6"></polygon><line x1="8" y1="2" x2="8" y2="18"></line><line x1="16" y1="6" x2="16" y2="22"></line></svg>
                        <span>Kontak</span>
                    </div>
                </a>
            </li>

        </ul>
        <div class="shadow-bottom"></div>

    </nav>

</div>
