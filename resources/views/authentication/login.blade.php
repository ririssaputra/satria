@extends('authentication.layout')
@section('content')
<h1 class="">Login Account</h1>
<!-- <p class="">Masuk Ke Akunmu Untuk Lanjut</p> -->
@if (session('alert'))
  <div class="alert alert-block alert-warning">
    <a class="close" data-dismiss="alert" href="#">×</a>
    {{ session('alert') }}<br>
  </div>
@endif

<form action="{{ url('/loginPost') }}" method="post" class="text-left">
  {{ csrf_field() }}
    <div class="form">

        <div id="username-field" class="field-wrapper input">
            <label for="username">E-mail Address</label>
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg>
            <input id="email" name="email" type="email" class="form-control" placeholder="@satria.com">
        </div>

        <div id="password-field" class="field-wrapper input mb-2">
            <div class="d-flex justify-content-between">
                <label for="password">Password</label>
                <!-- <a href="auth_pass_recovery_boxed.html" class="forgot-pass-link">Lupa Kata Sandi?</a> -->
            </div>
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-lock"><rect x="3" y="11" width="18" height="11" rx="2" ry="2"></rect><path d="M7 11V7a5 5 0 0 1 10 0v4"></path></svg>
            <input id="password" name="password" type="password" class="form-control" placeholder="Password">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" id="toggle-password" class="feather feather-eye"><path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle cx="12" cy="12" r="3"></circle></svg>
        </div>
        <div class="d-sm-flex justify-content-between">
            <div class="field-wrapper">
                <button type="submit" class="btn btn-primary" value="">Login</button>
            </div>
        </div>

        <p class="signup-link">Belum terdaftar ? <a href="{{ url('register')}}">Buat akunmu disini</a></p>

    </div>
</form>
@endsection
